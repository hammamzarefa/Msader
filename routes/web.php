<?php

use App\Http\Controllers\Admin\LogController;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
require __DIR__ . '/admin.php';
Route::get('/clear', function () {
    $output = new \Symfony\Component\Console\Output\BufferedOutput();
    Artisan::call('optimize:clear', array(), $output);
    return $output->fetch();
})->name('/clear');


Route::get('queue-work', function () {
    return Illuminate\Support\Facades\Artisan::call('queue:work', ['--stop-when-empty' => true]);
})->name('queue.work');
Route::get('schedule-run', function () {
    return Illuminate\Support\Facades\Artisan::call('schedule:run');
});
Route::get('migrate', function () {
    return Illuminate\Support\Facades\Artisan::call('migrate');
});

Route::get('/themeMode/{themeType?}', function ($themeType = 'true') {
    session()->put('dark-mode', $themeType);
    return $themeType;
})->name('themeMode');

Route::get('cron', 'FrontendController@cron')->name('cron');





Route::middleware('Maintenance')->group(function () {
    Route::get('/user', 'Auth\LoginController@showLoginForm')->name('login-form');

    Auth::routes(['verify' => true]);

    Route::group(['middleware' => ['guest']], function () {
        Route::get('register/{sponsor?}', 'Auth\RegisterController@sponsor')->name('register.sponsor');
    });


    Route::group(['middleware' => ['auth'], 'prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/check', 'VerificationController@check')->name('check');
        Route::get('/resend_code', 'VerificationController@resendCode')->name('resendCode');
        Route::post('/mail-verify', 'VerificationController@mailVerify')->name('mailVerify');
        Route::post('/sms-verify', 'VerificationController@smsVerify')->name('smsVerify');
        Route::post('/twoFA-Verify', 'VerificationController@twoFAverify')->name('twoFA-Verify');

        Route::middleware('userCheck')->group(function () {

            Route::get('/dashboard', 'HomeController@index')->name('home');

            Route::get('add-fund', 'HomeController@addFund')->name('addFund');
            Route::post('add-fund', 'PaymentController@addFundRequest')->name('addFund.request');
            Route::get('addFundConfirm', 'PaymentController@depositConfirm')->name('addFund.confirm');
            Route::post('addFundConfirm', 'PaymentController@fromSubmit')->name('addFund.fromSubmit');


            //transaction
            Route::get('/transaction', 'HomeController@transaction')->name('transaction');
            Route::get('/transaction-search', 'HomeController@transactionSearch')->name('transaction.search');
            Route::get('fund-history', 'HomeController@fundHistory')->name('fund-history');
            Route::get('fund-history-search', 'HomeController@fundHistorySearch')->name('fund-history.search');

            Route::get('/profile', 'HomeController@profile')->name('profile');
            Route::post('/updateProfile', 'HomeController@updateProfile')->name('updateProfile');
            Route::put('/updateInformation', 'HomeController@updateInformation')->name('updateInformation');
            Route::post('/updatePassword', 'HomeController@updatePassword')->name('updatePassword');

            Route::get('/apiKey', 'HomeController@apiKey')->name('apiKey');


            Route::get('/referral', 'HomeController@referral')->name('referral');
            Route::get('/referral-bonus', 'HomeController@referralBonus')->name('referral.bonus');
            Route::get('/referral-bonus-search', 'HomeController@referralBonusSearch')->name('referral.bonus.search');


            //order
            Route::resource('order', 'User\OrderController');
            // Route::post('order', 'User\OrderController@store')->name('user.order.store');;

            Route::get('/orders', 'User\OrderController@search')->name('order.search');
            Route::post('/order/status', 'User\OrderController@statusChange')->name('order.status.change');
            Route::get('/orders/{status}', 'User\OrderController@statusSearch')->name('order.status.search');
            Route::get('/mass/orders', 'User\OrderController@massOrder')->name('order.mass');
            Route::post('/mass/orders', 'User\OrderController@masOrderStore')->name('order.mass.store');
            Route::get('/get-service', 'ServiceController@getservice')->name('get.service');

            //order search
            Route::get('/services', 'User\ServiceController@index')->name('service.show');
            Route::get('/services/{id}', 'User\ServiceController@services')->name('category.services');
            Route::get('/service-search', 'User\ServiceController@search')->name('service.search');
            Route::get('/neworder/{id}', 'User\ServiceController@order')->name('category.order');


            Route::get('/api/docs', 'User\ApiController@index')->name('api.docs');
            Route::post('/keyGenerate', 'User\ApiController@apiGenerate')->name('keyGenerate');

            // TWO-FACTOR SECURITY
            Route::get('/twostep-security', 'HomeController@twoStepSecurity')->name('twostep.security');
            Route::post('twoStep-enable', 'HomeController@twoStepEnable')->name('twoStepEnable');
            Route::post('twoStep-disable', 'HomeController@twoStepDisable')->name('twoStepDisable');

            // Support Ticket
            Route::group(['prefix' => 'ticket', 'as' => 'ticket.'], function () {
                Route::get('/', 'User\SupportController@index')->name('list');
                Route::get('/create', 'User\SupportController@create')->name('create');
                Route::post('/create', 'User\SupportController@store')->name('store');
                Route::get('/view/{ticket}', 'User\SupportController@view')->name('view');
                Route::put('/reply/{ticket}', 'User\SupportController@reply')->name('reply');
                Route::get('/download/{ticket}', 'User\SupportController@download')->name('download');
            });

            Route::post('/service', function (Request $request) {
                $parent_id = $request->cat_id;
                $services = Service::where('category_id', $parent_id)->where('service_status', 1)->get();
                return response()->json($services);
            })->name('service');


            Route::get('push-notification-show', 'SiteNotificationController@show')->name('push.notification.show');
            Route::get('push.notification.readAll', 'SiteNotificationController@readAll')->name('push.notification.readAll');
            Route::get('push-notification-readAt/{id}', 'SiteNotificationController@readAt')->name('push.notification.readAt');

            Route::get('/user-service', 'User\OrderController@userservice')->name('service_id');

        });
    });

    Route::match(['get', 'post'], 'success', 'PaymentController@success')->name('success');
    Route::match(['get', 'post'], 'failed', 'PaymentController@failed')->name('failed');
    Route::match(['get', 'post'], 'payment/{code}/{trx?}/{type?}', 'PaymentController@gatewayIpn')->name('ipn');


    Route::get('/language/{code?}', 'FrontendController@language')->name('language');


    Route::get('/blog-details/{slug}/{id}', 'FrontendController@blogDetails')->name('blogDetails');
    Route::get('/blog', 'FrontendController@blog')->name('blog');

    Route::get('/', 'FrontendController@index')->name('home');
    Route::get('/about', 'FrontendController@about')->name('about');
    Route::get('/services', 'FrontendController@services')->name('services');
    Route::get('/service-search', 'FrontendController@serviceSearch')->name('service.search');


    Route::post('/subscribe', 'FrontendController@subscribe')->name('subscribe');


    Route::get('/faq', 'FrontendController@faq')->name('faq');
    Route::get('/api-docs', 'FrontendController@apiDocs')->name('apiDocs');
    Route::get('/contact', 'FrontendController@contact')->name('contact');
    Route::post('/contact', 'FrontendController@contactSend')->name('contact.send');
    Route::get('/{getLink}/{content_id}', 'FrontendController@getLink')->name('getLink');

});




