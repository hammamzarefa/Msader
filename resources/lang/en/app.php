<?php

return [
    'Sponsor By' => 'Sponsor by',
    'First Name' => 'First Name',
    'Last Name' => 'Last Name',
    'Username' => 'Username',
    'email' => 'email',
    'Your Phone Number' => 'Your Phone Number',
    'Password' => 'Password',
    'Confirm Password' => 'Password',
    'I Agree with the Terms & conditions' => 'I Agree with the Terms & conditions',
    'sign up' => 'sign up',
    'Login here' => 'Login here',
];
