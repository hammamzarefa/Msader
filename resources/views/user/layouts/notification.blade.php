
@if (session()->has('success'))
    <script>
        Notiflix.Notify.Success({!! json_encode(trans(session('success'))) !!}, { timeout: 5000 });
    </script>
@endif

@if (session()->has('error'))
    <script>
        Notiflix.Notify.Failure({!! json_encode(trans(session('error'))) !!}, { timeout: 5000 });
    </script>
@endif

@if (session()->has('warning'))
    <script>
        Notiflix.Notify.Warning({!! json_encode(trans(session('warning'))) !!}, { timeout: 5000 });
    </script>
@endif

@if ($errors->any())
    @php
        $collection = collect($errors->all());
        $errors = $collection->unique();
    @endphp
    <script>
        "use strict";
        @foreach ($errors as $error)
            Notiflix.Notify.Failure({!! json_encode(trans($error)) !!}, { timeout: 5000 });
        @endforeach
    </script>
@endif

