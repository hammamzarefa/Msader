<!-- whatsapp -->
<a href="https://wa.me/{{config('basic.whatsapp')}}" target="_blank" class="whatsapp">
    <div class="whatsapp-bg">
            <span>
                <i class="fab fa-whatsapp"></i>
            </span>
    </div>
    <div class="drops">
        <div class="drop1"></div>
        <div class="drop2"></div>
    </div>
</a>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="position: absolute;z-index: -1;">
    <defs>
        <filter id="liquid">
            <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="liquid" />
        </filter>
    </defs>
</svg>
<!-- whatsapp-end -->
