<!-- header-area -->
<header>
    <div class="menu-area">
        <div class="container">
            <div class="mobile-nav-toggler"><i class="fas fa-bars"></i></div>
            <div class="menu-wrap">
                <nav class="menu-nav">
                    <div class="logo d-flex d-md-none m-auto"><a href="{{route('home')}}">
                            <img src="{{asset('assets/themes/user-v2/img/logo/logo.png')}}" alt=""></a>
                    </div>
                    <div class="navbar-wrap main-menu d-flex d-md-none" style="flex-grow: 0;">
                        <ul class="navigation">
                            @auth
                                <li>
                                    <i class="fi-sr-crown"></i>
                                    <strong>{{auth()->user()->balance}}</strong>
                                </li>
                            @endauth
                        </ul>
                    </div>
                    <div class="navbar-wrap main-menu d-none d-lg-flex">
                        <ul class="navigation">
                            <li class="active d-none d-lg-flex">
                                <a href="{{route('home')}}" class="logo m-0"><img
                                        src="{{asset('assets/themes/user-v2/img/logo/logo.png')}}" alt=""></a>
                            </li>
                            <li><a href="{{route('home')}}">@lang('home')</a></li>
                            {{--                            <li><a href="{{route('blog')}}">@lang('Blog')</a></li>--}}
                            {{--                            <li><a href="{{route('about')}}">@lang('About us')</a></li>--}}
                            {{--                            <li><a href="{{route('contact')}}">@lang('Contact us') </a></li>--}}

                            {{--                            @auth--}}
                            {{--                                <li><a href="{{route('user.home')}}">@lang('dashboard')</a></li>--}}
                            {{--                            @endauth--}}

                            @auth
                                <li class="menu-item-has-children ">
                                    <a href="#" style="font-size: 20px;">
                                        {{auth()->user()->username}}
                                        <i class="fi-sr-user"></i>
                                    </a>
                                    <ul class="submenu">
                                        <li><a href="{{ route('user.profile') }}"><i
                                                    class="fi-sr-settings m-1"></i>@lang('My Profile')</a></li>
                                        {{--                                        <li><a href="{{ route('user.referral') }}"><i--}}
                                        {{--                                                    class="fi-sr-database m-1"></i>@lang('app.My Referral')</a></li>--}}
                                        {{--                                        <li><a href="{{ route('user.referral.bonus') }}"><i--}}
                                        {{--                                                    class="fi-sr-crown m-1"></i>@lang('app.Referral Bonus')</a></li>--}}
                                        {{--                                        <li><a href="{{ route('user.ticket.create') }}"><i--}}
                                        {{--                                                    class="fi-sr-credit-card m-1"></i>@lang('app.Open Ticket')</a></li>--}}
                                        {{--                                        <li><a href="{{ route('user.ticket.list') }}"><i--}}
                                        {{--                                                    class="fi-sr-credit-card m-1"></i>@lang('app.Show Ticket')</a></li>--}}
                                        {{--                                        <li><a href="{{route('user.twostep.security')}}"><i--}}
                                        {{--                                                    class="fi-sr-key m-1"></i>@lang('app.2FA Security')</a></li>--}}

                                        <li>

                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                                <i class="fi-sr-sign-out m-1"></i>
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  class="d-none">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <i class="fi-sr-crown"></i>
                                    <strong>{{Auth()->user()->balance}}</strong>
                                </li>

                                <li class="notflication push-notification dropdown" id="pushNotificationArea">
                                    <a class="nav-link dropdown-toggle pl-md-3 position-relative"
                                       href="javascript:void(0)"
                                       id="bell" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="fi-sr-bell"></i>
                                        <span class="notification_count" v-cloak>@{{ items.length }}</span>
                                    </a>

                                    <div
                                        class="right-dropdown dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                                        <ul class="list-style-none">
                                            <li>
                                                <div class="scrollable message-center notifications position-relative">

                                                    <!-- Message -->
                                                    <a v-for="(item, index) in items"
                                                       @click.prevent="readAt(item.id, item.description.link)"
                                                       href="javascript:void(0)"
                                                       class="message-item d-flex align-items-center justify-content-between border-bottom px-3 py-2">


                                                        <div class="d-inline-block v-middle pl-2">
                                                                        <span class="font-12  d-block text-muted"
                                                                              v-cloak
                                                                              v-html="item.description.text"></span>
                                                            <span class="font-12  d-block text-muted text-truncate"
                                                                  v-cloak>@{{ item.formatted_date }}</span>
                                                        </div>
                                                        <span
                                                            class="btn btn-success text-white rounded-circle btn-circle">
                                                                                    <i :class="item.description.icon"
                                                                                       class="text-white"></i>
                                                                                </span>
                                                    </a>

                                                </div>
                                            </li>

                                            <li>
                                                <a class="nav-link pt-3 text-center text-dark notification-clear-btn"
                                                   href="javascript:void(0);"
                                                   v-if="items.length > 0" @click.prevent="readAll">
                                                    <strong>@lang('Clear all')</strong>
                                                </a>
                                                <a class="nav-link pt-3 text-center text-dark"
                                                   href="javascript:void(0);"
                                                   v-else>
                                                    <strong>@lang('No notification')</strong>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>

                                </li>
                            @else
                                <li class=""><a href="{{route('login')}}">
                                        <i class="fi-sr-user"></i>
                                        <strong>@lang('login')</strong>
                                    </a></li>
                            @endauth
                        </ul>
                    </div>
                    <div class="header-action d-none d-md-block">
                        <ul>
                            <li class="header-btn"><a href="https://wa.me/{{config('basic.whatsapp')}}"
                                                      target="_blank" class="btn">@lang('Contact us')</a></li>
                        </ul>

                    </div>
                </nav>
            </div>
            <!-- Mobile Menu  -->
            <div class="mobile-menu">
                <nav class="menu-box">
                    <div class="close-btn"><i class="fas fa-times"></i></div>
                    <div class="nav-logo">
                        <a href="{{route('home')}}">
                            <img
                                src="{{asset('assets/themes/user-v2/img/logo/logo.png')}}" alt="">
                        </a>
                    </div>

                    <div class="menu-outer">
                        <ul class="navigation">
                            @auth
                                <li><a href="{{route('user.home')}}">@lang('home')</a></li>
                            @else
                                <li><a href="{{route('home')}}">@lang('home')</a></li>
                            @endauth
                            {{--                                                        <li><a href="">المقالات</a></li>--}}
                            {{--                            <li><a href="">من نحن</a></li>--}}
                            {{--                            <li><a href="">تواصل معنا </a></li>--}}

                            <li class="active d-none d-lg-flex">
                                <a href="{{route('home')}}" class="logo m-0"><img
                                        src="{{asset('assets/themes/user-v2/img/logo/logo.png')}}" alt=""></a>
                            </li>
                            @auth
                                <li class=" menu-item-has-children">
                                    <a href="#" style="font-size: 20px;">
                                        {{Auth()->user()->username}}
                                        <i class="fi-sr-user"></i>
                                    </a>
                                    <ul class="submenu">
                                        <li><a href="{{ route('user.profile') }}"><i
                                                    class="fi-sr-settings m-1"></i>@lang('My Profile')</a></li>
                                        {{--                                        <li><a href="{{ route('user.referral') }}"><i--}}
                                        {{--                                                    class="fi-sr-database m-1"></i>@lang('app.My Referral')</a></li>--}}
                                        {{--                                        <li><a href="{{ route('user.referral.bonus') }}"><i--}}
                                        {{--                                                    class="fi-sr-crown m-1"></i>@lang('app.Referral Bonus')</a></li>--}}
                                        {{--                                        <li><a href="{{ route('user.ticket.create') }}"><i--}}
                                        {{--                                                    class="fi-sr-credit-card m-1"></i>@lang('app.Open Ticket')</a></li>--}}
                                        {{--                                        <li><a href="{{ route('user.ticket.list') }}"><i--}}
                                        {{--                                                    class="fi-sr-credit-card m-1"></i>@lang('app.Show Ticket')</a></li>--}}
                                        {{--                                        <li><a href="{{route('user.twostep.security')}}"><i--}}
                                        {{--                                                    class="fi-sr-key m-1"></i>@lang('app.2FA Security')</a></li>--}}

                                        <li>

                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                                <i class="fi-sr-sign-out m-1"></i>
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  class="d-none"> --}
                                                }
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <i class="fi-sr-crown"></i>
                                    <strong>{{Auth()->user()->balance}}</strong>
                                </li>
                            @else
                                <li class=""><a href="{{route('login')}}">
                                        <i class="fi-sr-user"></i>
                                        <strong>@lang('login')</strong>
                                    </a></li>
                            @endauth

                        </ul>
                    </div>
                    <div class="social-links">
                        <ul class="clearfix">
                            <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                            <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                            <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                            <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                        </ul>
                    </div>

                </nav>
            </div>
            <div class="menu-backdrop"></div>
            <!-- End Mobile Menu -->
        </div>
    </div>
</header>
<!-- header-area-end -->
@push('js')
    @auth
        <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
        <script>
            'use strict';
            let pushNotificationArea = new Vue({
                el: "#pushNotificationArea",
                data: {
                    items: [],
                },
                mounted() {
                    this.getNotifications();
                    this.pushNewItem();
                },
                methods: {
                    getNotifications() {
                        let app = this;
                        axios.get("{{ route('user.push.notification.show') }}")
                            .then(function (res) {
                                app.items = res.data;
                            })
                    },
                    readAt(id, link) {
                        let app = this;
                        let url = "{{ route('user.push.notification.readAt', 0) }}";
                        url = url.replace(/.$/, id);
                        axios.get(url)
                            .then(function (res) {
                                if (res.status) {
                                    app.getNotifications();
                                    if (link != '#') {
                                        window.location.href = link
                                    }
                                }
                            })
                    },
                    readAll() {
                        let app = this;
                        let url = "{{ route('user.push.notification.readAll') }}";
                        axios.get(url)
                            .then(function (res) {
                                if (res.status) {
                                    app.items = [];
                                }
                            })
                    },
                    pushNewItem() {
                        let app = this;
                        // Pusher.logToConsole = true;
                        let pusher = new Pusher("{{ env('PUSHER_APP_KEY') }}", {
                            encrypted: true,
                            cluster: "{{ env('PUSHER_APP_CLUSTER') }}"
                        });
                        let channel = pusher.subscribe('user-notification.' + "{{ Auth::id() }}");
                        channel.bind('App\\Events\\UserNotification', function (data) {
                            app.items.unshift(data.message);
                        });
                        channel.bind('App\\Events\\UpdateUserNotification', function (data) {
                            app.getNotifications();
                        });
                    }
                }
            });
        </script>
    @endauth
@endpush



