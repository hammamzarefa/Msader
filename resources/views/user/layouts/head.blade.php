<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
@include('partials.seo')

@stack('style-lib')

<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/bootstrap.min.css')}}">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/fontawesome-all.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/uicons-solid-rounded.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/jquery.mCustomScrollbar.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/slick.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/default.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/style.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/style-new.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/loading.css')}}">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/css/responsive.css')}}">
<!-- Arabic Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bitter:ital@1&family=Cairo:wght@500&family=Ruwudu:wght@500&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/themes/user-v2/package/swiper-bundle.min.css')}}">


@stack('style')

@stack('extra-style')
