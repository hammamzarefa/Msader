<!-- sidebar -->
<div id="sidebar" class="sidebar">
    <div id="sidebar-btn" class="sidebar-logo m-auto">
        <a href="{{route('user.home')}}"> <img class="w-50" style="cursor: pointer;" src="{{asset('assets/themes/user-v2/img/logo/logo-icon.png')}}"
                                   alt=""></a>
    </div>
    <div class="sidebar-icon">
        <ul>
            <li @class(['active'=> request()->routeIs('user.home')])>
                <a href="{{ route('user.home') }}" class="icon"><i class="fi-sr-home"></i></a>
                <a href="{{ route('user.home') }}" class="name-link">لوحة التحكم</a>
            </li>
            <li @class(['active'=> request()->routeIs('user.service.*')])>
                <a href="{{ route('user.service.show') }}" class="icon"><i class="fi-sr-apps-delete"></i></a>
                <a href="{{ route('user.service.show') }}" class="name-link">الباقات</a>
            </li>
            <li @class(['active'=> request()->routeIs('user.order.*')])>
                <a href="{{ route('user.order.index') }}" class="icon"><i class="fi-sr-butterfly"></i></a>
                <a href="{{ route('user.order.index') }}" class="name-link">طلباتي</a>
            </li>
{{--            <li @class(['active'=> request()->routeIs('user.home')])>--}}
{{--                <a href="blog.html" class="icon"><i class="fi-sr-camping"></i></a>--}}
{{--                <a href="blog.html" class="name-link">المقالات</a>--}}
{{--            </li>--}}
            <li @class(['active'=> request()->routeIs('user.addFund')])>
                <a href="{{route('user.addFund')}}" class="icon"><i class="fi-sr-crown"></i></a>
                <a href="{{route('user.addFund')}}" class="name-link">شحن رصيد</a>
            </li>
            <li @class(['active'=> request()->routeIs('user.fund-history')])>
                <a href="{{ route('user.fund-history') }}" class="icon"><i class="fi-sr-database"></i></a>
                <a href="{{ route('user.fund-history') }}" class="name-link">دفعاتي</a>
            </li>
            <li @class(['active'=> request()->routeIs('user.transaction.*')])>
                <a href="{{ route('user.transaction') }}" class="icon"><i class="fi-sr-book-alt"></i></a>
                <a href="{{ route('user.transaction') }}" class="name-link">المعاملات</a>
            </li>
{{--            <li @class(['active'=> request()->routeIs('user.home')])>--}}
{{--                <a href="#" class="icon"><i class="fi-sr-credit-card"></i></a>--}}
{{--                <a href="#" class="name-link">بطاقة الدعم</a>--}}
{{--            </li>--}}
{{--            <li @class(['active'=> request()->routeIs('user.home')])>--}}
{{--                <button style="--clr:#e039fd" class="btn-sideBar"><span>أنقر</span><i></i></button>--}}
{{--            </li>--}}
            <!-- <li>
                <a href="#" class="icon"><i class="fi-sr-user"></i></a>
                <a href="#" class="name-link">alaamhna3354</a>
            </li> -->
        </ul>
    </div>
</div>
<!-- sidebar-end -->
