<footer>
    <div class="copyright-wrap">
        <div class="container ">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="copyright-text text-center">
                        <p>{{trans('Copyright')}} © {{date('Y')}}  <a href="#">{{trans(config('basic.site_title'))}}</a></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
