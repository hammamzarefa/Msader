<!DOCTYPE html>
<html dir="rtl" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('user.layouts.head')
{{--    @livewireStyles--}}
</head>
<body class="lang-{{ str_replace('_', '-', app()->getLocale()) }}">
@include('user.layouts.whatsapp')
@include('user.layouts.preloader')


<div class="overlay"></div>

@include('user.layouts.sidebar')

<!-- Offcanvas-area -->

<!-- Offcanvas-area-end -->


<div class="main-content">

    @include('user.layouts.header')

    <main>
        @yield('content')
    </main>

    {{--    @include('user.layouts.side-notify')--}}

    @include('user.layouts.notification')
    @include('user.layouts.footer')


</div>


<!-- JS here -->
<script src="{{asset('assets/themes/user-v2/js/vendor/jquery-3.6.0.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/bootstrap.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/isotope.pkgd.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/imagesloaded.pkgd.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/jquery.magnific-popup.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/jquery.mCustomScrollbar.concat.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/slick.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/wow.min.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/plugins.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/js/main.js') }}" defer></script>
<script src="{{asset('assets/themes/user-v2/package/swiper-bundle.min.js') }}" defer></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js" defer></script>
<script src="{{asset('assets/global/js/notiflix-aio-2.7.0.min.js')}}" defer></script>
<script>
    AOS.init();
</script>

@stack('js-lib')


<script src="{{ asset('assets/global/js/axios.min.js') }}" defer></script>
<!-- <script src="{{ asset('assets/global/js/vue.min.js') }}" defer></script> -->
<script src="{{ asset('assets/global/js/pusher.min.js') }}" defer></script>
{{--<livewire:scripts/>--}}
@stack('js')
@include('user.layouts.notification')

</body>
</html>
