<div>
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

        @if(count($services))
            <div id="show-orders">
                <section class="inner-explore-products content">
                    <div class="container">
                        <div class="row justify-content-center services">
                            @foreach($services as $service)

                                <div @class([
                                    "col-xl-3 col-lg-4 col-sm-6 col-6 orders-item" => $current_service == null,
                                    "orders-item col-sm-6 col-12" => $current_service == $service->id,
                                    "d-none" => $current_service != null && $current_service != $service->id,
                            ]) id="{{$service->id}}">
                                    <div @class([
                                    'wrapper-card ' ,
                                    'active' => $current_service == $service->id,
                            ]) >
                                        <div class="single-card ">
                                            <div class="front ">
                                                <div class="top-collection-item">
                                                    <div class="collection-item-top">
                                                        <ul>
                                                            <li class="avatar">
                                                                {{--                                                        <strong--}}
                                                                {{--                                                            style="font-size: 20px;margin-inline-start: 10px;"--}}
                                                                {{--                                                            class="text-white">اختر باقة</strong>--}}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="collection-item-thumb">
                                                        <img
                                                            src="{{ getFile(config('location.category.path').$category->image) }}"
                                                            alt="">
                                                    </div>
                                                    <div class="collection-item-content">
                                                        <h5 class="title">
                                                            <a> {{$service->service_title}} </a>
                                                            <span class="price"
                                                                  data-value="{{$service->user_rate ?? $service->price}}">@lang(config('basic.currency_symbol')){{$service->user_rate ?? $service->price}}</span>
                                                        </h5>
                                                        <hr>
                                                        <h5 class="title">
                                                            <a>Max : <span
                                                                    class="Max m-0">{{$service->max_amount}}</span></a>
                                                            <a>Min : <span
                                                                    class="Min m-0">{{$service->min_amount}}</span></a>
                                                        </h5>
                                                    </div>
                                                    <div class="collection-item-bottom">
                                                        <ul>
                                                            <li
                                                                class=" m-auto btn next"
                                                                wire:click="setCurrentTo({{$service->id}})">
                                                                أطلب الان
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="back ">
                                                <div class="top-collection-item ">
                                                    <div class="water"></div>
                                                    <div class="collection-item-top">
                                                        <ul>
                                                            <li class="avatar">
                                                                <strong
                                                                    style="font-size: 20px;margin-inline-start: 10px;"
                                                                    class="text-white">عملية الطلب</strong>
                                                            </li>
                                                            <li class="Prev p-3 prev"
                                                                wire:click="setCurrentTo({{null}})">
                                                                <i style="color:#fff;" class="fas fa-arrow-right"></i>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div
                                                        class="mt-5 collection-item-thumb d-flex align-items-center justify-content-between">
                                                        <h5>السعر</h5>
                                                        <h5 style="color: #fff">@lang(config('basic.currency_symbol'))
                                                            <span
                                                                class="main-price"
                                                                data-price="{{$service->user_rate ?? $service->price}}">{{$service->user_rate ?? $service->price}}</span>
                                                        </h5>
                                                    </div>
                                                    <form wire:submit.prevent="createOrder" class="create-item-form">
                                                        <div class="form-grp">
                                                            <input id="number" name="player_number"
                                                                   wire:model="player_number"
                                                                   type="text"
                                                                   placeholder="رقم اللاعب">
                                                            @if($errors->has('player_number'))
                                                                <span
                                                                    class="error">{{ $errors->first('player_number') }}</span>
                                                            @endif

                                                        </div>
                                                        <div class="form-grp">
                                                            <input class="qty" type="number" name="quantity"
                                                                   wire:model="quantity"
                                                                   onkeypress="return isNumberKey(event)"
                                                                   placeholder="الكمية">
                                                            @if($errors->has('quantity'))
                                                                <span
                                                                    class="error">{{ $errors->first('quantity') }}</span>
                                                            @endif
                                                        </div>

{{--                                                        <div class="form-grp">--}}
{{--                                                            <input class="qty" type="url" name="link" wire:model="link"--}}

{{--                                                                   placeholder="رابط">--}}
{{--                                                            @if($errors->has('link'))--}}
{{--                                                                <span class="error">{{ $errors->first('link') }}</span>--}}
{{--                                                            @endif--}}
{{--                                                        </div>--}}
{{--                                                        <div class="form-grp">--}}
{{--                                                            <input class="qty" type="text" name="runs" wire:model="runs"--}}
{{--                                                                   placeholder="runs">--}}
{{--                                                            @if($errors->has('runs'))--}}
{{--                                                                <span class="error">{{ $errors->first('runs') }}</span>--}}
{{--                                                            @endif--}}
{{--                                                        </div>--}}
{{--                                                        <div class="form-grp">--}}
{{--                                                            <input class="qty" type="text" name="interval"--}}
{{--                                                                   wire:model="interval"--}}
{{--                                                                   placeholder="interval">--}}
{{--                                                            @if($errors->has('interval'))--}}
{{--                                                                <span--}}
{{--                                                                    class="error">{{ $errors->first('interval') }}</span>--}}
{{--                                                            @endif--}}
{{--                                                        </div>--}}
                                                        <div class="collection-item-bottom">
                                                            <ul>
                                                                <li class="m-auto">
                                                                    <button type="submit" class="btn-form-order">
                                                                        <span>أطلب الآن</span>
                                                                    </button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </section>
            </div>
        @endif



</div>

@push('js')
    <script>
        function initSwiper(next) {
            let allWorks = document.querySelectorAll('.swiper11');
            allWorks.forEach(function (item) {
                let x = new Swiper(item, {
                    loop: false,
                    speed: 1000,
                    effect: "cube",
                    simulateTouch: false,

                    navigation: {
                        nextEl: '.next',
                        prevEl: '.prev',
                    }

                });
                if (next) {
                    x.slideNext();
                }
            })
        }

        function destroySwiper() {
            let allWorks = document.querySelectorAll('.swiper11');
            allWorks.forEach(function (item) {
                if (item.swiper) item.swiper.destroy(true, true);
            })
        }

        window.addEventListener('init', event => {
            initSwiper(event.detail.next);
        })
        window.addEventListener('destroy', event => {
            destroySwiper();
        })
        window.addEventListener('xx', event => {
            $('#btn-hide-order').addClass("show");
            $("#show-products").hide(500);
            $("#show-orders").show(500);
        })


        window.addEventListener('back', event => {
            back();
        })

        function back() {
            $('#btn-hide-order').removeClass("show");
            $("#show-orders").hide(500);
            $("#show-products").show(500);
        }


        window.addEventListener('hideOther', event => {
            $('.services > div:not(#' + event.detail.id + ')').hide("slow");
            setTimeout(() => {
                $('.services > div:not(#' + event.detail.id + ')').removeAttr('class');
                $('.services > div:not(#' + event.detail.id + ')').addClass("orders-item");
                $('.services > div:not(#' + event.detail.id + ')').addClass("col-12");
                $('.services > div:not(#' + event.detail.id + ')').addClass("col-sm-6");
            }, 500);
        })
        window.addEventListener('showAll', event => {
            $('.services > div').show("slow");


            setTimeout(() => {
                $('.services > div').removeAttr('class');
                $('.services > div').removeAttr('class');
                $('.services > div').addClass("orders-item");
                $('.services > div').addClass("col-sm-6");
                $('.services > div').addClass("col-lg-4");
                $('.services > div').addClass("col-xl-3");
            }, 500);
        })

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>

@endpush
