@extends('user.layouts.app')
@section('title')
    @lang($title)
@endsection
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>

    <div class="category-area">
        <div class="container">
            <form action="{{route('user.referral.bonus.search')}}" method="get">
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <div class="sidebar-search">
                            <input type="text" placeholder="@lang('app.Search User')" name="search_user"
                                   value="{{@request()->search_user}}">


                        </div>
                    </div>
                    <div class="col-12 col-sm-4 ">
                        <div class="sidebar-search">
                            <input type="date" class="form-control" name="datetrx" id="datepicker">
                        </div>
                    </div>


                    <div class="col-12 col-sm-2">
                        <button type="submit" class="btn"
                                style="padding-left: 30px;padding-right: 30px;height: 100%;">بحث <i
                                class="fa fa-search"></i></button>
                    </div>

                </div>
            </form>
        </div>
    </div>


    <div class="container mt-5">
        <div class="activity-table-wrap">
            <div class="activity-table-nav">
                <h4 class="">الطلبات</h4>

            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="nft" role="tabpanel" aria-labelledby="nft-tab">
                    <div class="activity-table-responsive">
                        <table class="table activity-table">
                            <thead>
                            <tr>
                                <th>@lang('app.SL No.')</th>
                                <th>@lang('app.Bonus From')</th>
                                <th>@lang('app.Amount')</th>
                                <th>@lang('app.Remarks')</th>
                                <th>@lang('app.Time')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($transactions as $transaction)
                                <tr>
                                    <td data-label="@lang('SL No.')">
                                        {{loopIndex($transactions) + $loop->index}}</td>
                                    <td data-label="@lang('Bonus From')">@lang(optional($transaction->bonusBy)->fullname)</td>
                                    <td data-label="@lang('Amount')">
                                                                                <span
                                                                                    class="font-weight-bold text-success">{{getAmount($transaction->amount, config('basic.fraction_number')). ' ' . trans(config('basic.currency'))}}</span>
                                    </td>

                                    <td data-label="@lang('Remarks')"> @lang($transaction->remarks)</td>
                                    <td data-label="@lang('Time')">
                                        {{ dateTime($transaction->created_at, 'd M Y h:i A') }}
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="100%">{{trans('app.No Data Found!')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $transactions->appends($_GET)->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@push('extra-script')
@endpush
