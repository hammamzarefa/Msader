@extends('user.layouts.app')
@section('title')
    @lang('app.Fund History')
@endsection
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>


    <div class="category-area">
        <div class="container">
            <form action="{{ route('user.fund-history.search') }}" method="get">
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <div class="sidebar-search">
                            <input type="text" placeholder="@lang('app.Type Here')" name="name"
                                   value="{{@request()->name}}">


                        </div>
                    </div>
                    <div class="col-12 col-sm-3 ">
                        <div class="select">
                            <select name="status">
                                <option value="">@lang('app.All Payment')</option>
                                <option value="1"
                                        @if(@request()->status == '1') selected @endif>@lang('app.Complete Payment')</option>
                                <option value="2"
                                        @if(@request()->status == '2') selected @endif>@lang('app.Pending Payment')</option>
                                <option value="3"
                                        @if(@request()->status == '3') selected @endif>@lang('app.Cancel Payment')</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 ">
                        <div class="sidebar-search">
                            <input type="date" class="form-control" name="date_time" id="datepicker">
                        </div>
                    </div>

                    <div class="col-12 col-sm-2">
                        <button type="submit" class="btn"
                                style="padding-left: 30px;padding-right: 30px;height: 100%;">بحث <i
                                class="fa fa-search"></i></button>
                    </div>

                </div>
            </form>
        </div>
    </div>


    <div class="container mt-5">
        <div class="activity-table-wrap">
            <div class="activity-table-nav">
                <h4 class="">الطلبات</h4>

            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="nft" role="tabpanel" aria-labelledby="nft-tab">
                    <div class="activity-table-responsive">
                        <table class="table activity-table">
                            <thead>
                            <tr>
                                <th scope="col">@lang('app.Transaction ID')</th>
                                <th scope="col">@lang('app.Gateway')</th>
                                <th scope="col">@lang('app.Amount')</th>
                                <th scope="col">@lang('app.Charge')</th>
                                <th scope="col">@lang('app.Status')</th>
                                <th scope="col">@lang('app.Time')</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse($funds as $data)
                                <tr>

                                    <td data-label="#@lang('Transaction ID')">{{$data->transaction}}</td>
                                    <td data-label="@lang('Gateway')">@lang(optional($data->gateway)->name)</td>
                                    <td data-label="@lang('Amount')">
                                        <strong>{{getAmount($data->amount)}} @lang($basic->currency)</strong>
                                    </td>

                                    <td data-label="@lang('Charge')">
                                        <strong>{{getAmount($data->charge)}} @lang($basic->currency)</strong>
                                    </td>

                                    <td data-label="@lang('Status')">
                                        @if($data->status == 1)
                                            <span class="badge badge-success badge-pill">@lang('Complete')</span>
                                        @elseif($data->status == 2)
                                            <span class="badge badge-warning badge-pill">@lang('Pending')</span>
                                        @elseif($data->status == 3)
                                            <span class="badge badge-danger badge-pill">@lang('Cancel')</span>
                                        @endif
                                    </td>

                                    <td data-label="@lang('Time')">
                                        {{ dateTime($data->created_at, 'd M Y h:i A') }}
                                    </td>
                                </tr>

                            @empty
                                <tr class="text-center">
                                    <td colspan="100%">{{trans('app.No Data Found!')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $funds->appends($_GET)->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
