@extends('user.layouts.app')
@section('title')
    @lang('Add Fund')
@endsection
@section('content')
    <div class="area-bg padding-header">

        <section class="creator-area pt-80 pb-80" style="direction: ltr;">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title mb-35">
                            <h2 class="title" style="justify-content:flex-end"> @lang('Add Fund') <img
                                    src="{{asset('assets/themes/user-v2/img/icons/title_icon01.png')}}" alt=""></h2>
                        </div>

                    </div>

                    @foreach($gateways as $key => $gateway)
                        <div class="creator-item col-5 col-sm-2">
                            <img src="{{asset('assets/themes/user-v2/img/icons/start.png')}}" alt="{{$gateway->name}}"
                                 class="star">
                            <div class="creator-thumb">
                                <img src="{{ getFile(config('location.gateway.path').$gateway->image)}}" alt="">
                            </div>
                            <div class="creator-content">
                                <button class="btn addFund"
                                        data-id="{{$gateway->id}}"
                                        data-name="{{$gateway->name}}"
                                        data-currency="{{$gateway->currency}}"
                                        data-gateway="{{$gateway->code}}"

                                        data-min_amount="{{getAmount($gateway->min_amount, $basic->fraction_number)}}"
                                        data-max_amount="{{getAmount($gateway->max_amount,$basic->fraction_number)}}"
                                        data-percent_charge="{{getAmount($gateway->percentage_charge,$basic->fraction_number)}}"
                                        data-fix_charge="{{getAmount($gateway->fixed_charge, $basic->fraction_number)}}"


                                        data-bs-toggle="modal" data-bs-target="#addFund">@lang('Pay Now')</button>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </section>
        <!-- Button trigger modal -->
        <!-- Modal -->
        <div class="modal fade" id="addFund" tabindex="1000" aria-labelledby="addFundLabel" aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title method-name " id="exampleModalLabel">Payment By Bank Transfer - USD</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                      d="M5 19L19 4.99996"></path>
                                <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                      d="M19 19L4.99996 4.99996"></path>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body ">
                        <div class="payment-form">
                            <p class="depositLimit">
                                Transaction Limit: 10.00 - 10,000.00 $

                            </p>
                            <p class="depositCharge">
                                Charge: 0.00 $
                            </p>

                            <div class="sidebar-search">
                                <form action="#">
                                    <input type="hidden" class="form-control gateway" name="gateway" value="">
                                    <input type="text" class="amount" name="amount" value="">
                                    <button type="submit" onclick="event.preventDefault();" class="show-currency"> USD
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="payment-info text-center">--}}
                            <img id="loading" src="{{asset('assets/images/loading.gif')}}" alt=""/>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn checkCalc" style="padding-left: 30px;padding-right: 30px;">
                            Check
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection



@push('js')

    <script>
        $('#loading').hide();
        "use strict"
        var id, minAmount, maxAmount, baseSymbol, fixCharge, percentCharge, currency, amount, gateway;

        $('.addFund').on('click', function () {
            id = $(this).data('id');
            gateway = $(this).data('gateway');
            minAmount = $(this).data('min_amount');
            maxAmount = $(this).data('max_amount');
            baseSymbol = "{{config('basic.currency_symbol')}}";
            fixCharge = $(this).data('fix_charge');
            percentCharge = $(this).data('percent_charge');
            currency = $(this).data('currency');
            $('.depositLimit').text(`@lang('Transaction Limit:') ${minAmount} - ${maxAmount}  ${baseSymbol}`);

            var depositCharge = `@lang('Charge:') ${fixCharge} ${baseSymbol}  ${(0 < percentCharge) ? ' + ' + percentCharge + ' % ' : ''}`;
            $('.depositCharge').text(depositCharge);

            $('.method-name').text(`@lang('Payment By') ${$(this).data('name')} - ${currency}`);
            $('.show-currency').text("{{config('basic.currency')}}");
            $('.gateway').val(currency);
        });


        $(".checkCalc").on('click', function () {
            $('#loading').show();
            $('.payment-form').addClass('d-none');

            amount = $('.amount').val();

            $.ajax({
                url: "{{route('user.addFund.request')}}",
                type: 'POST',
                data: {
                    amount,
                    gateway
                },
                success(data) {

                    $('.payment-form').addClass('d-none');
                    $('.checkCalc').closest('.modal-footer').addClass('d-none');

                    var htmlData = `
                             <ul class="list-group text-center">
                                <li class="list-group-item">
                                    <img src="${data.gateway_image}"
                                        style="max-width:100px; max-height:100px; margin:0 auto;"/>
                                </li>
                                <li class="list-group-item">
                                    @lang('Amount'):
                                    <strong>${data.amount} </strong>
                                </li>
                                <li class="list-group-item">@lang('Charge'):
                                        <strong>${data.charge}</strong>
                                </li>
                                <li class="list-group-item">
                                    @lang('Payable'): <strong> ${data.payable}</strong>
                                </li>
                                <li class="list-group-item">
                                    @lang('Conversion Rate'): <strong>${data.conversion_rate}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>${data.in}</strong>
                                </li>

                                ${(data.isCrypto == true) ? `
                                <li class="list-group-item">
                                    ${data.conversion_with}
                                </li>
                                ` : ``}

                                <li class="list-group-item">
                                <a href="${data.payment_url}" class="btn btn-default text-white waves-effect waves-light btn-block addFund ">@lang('Pay Now')</a>
                                </li>
                                </ul>`;

                    $('.payment-info').html(htmlData)
                    $('.payment-info').removeClass('d-none');
                },
                complete: function (e, f, x) {
                    var data = e.responseJSON;
                    console.log('c');
                    var htmlData = `
                             <ul class="list-group text-center">
                                <li class="list-group-item">
                                    <img src="${data.gateway_image}"
                                        style="max-width:100px; max-height:100px; margin:0 auto;"/>
                                </li>
                                <li class="list-group-item">
                                    @lang('Amount'):
                                    <strong>${data.amount} </strong>
                                </li>
                                <li class="list-group-item">@lang('Charge'):
                                        <strong>${data.charge}</strong>
                                </li>
                                <li class="list-group-item">
                                    @lang('Payable'): <strong> ${data.payable}</strong>
                                </li>
                                <li class="list-group-item">
                                    @lang('Conversion Rate'): <strong>${data.conversion_rate}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>${data.in}</strong>
                                </li>

                                ${(data.isCrypto == true) ? `
                                <li class="list-group-item">
                                    ${data.conversion_with}
                                </li>
                                ` : ``}

                                <li class="list-group-item">
                                <a href="${data.payment_url}" class="btn btn-default text-white waves-effect waves-light btn-block addFund ">@lang('Pay Now')</a>
                                </li>
                                </ul>`;
                    $('#loading').hide();
                    $('.payment-info').html(htmlData)
                    $('.payment-info').removeClass('d-none');

                },
                error(err) {
                    console.log('sd');
                    var errors = err.responseJSON;
                    for (var obj in errors) {
                        $('.errors').text(`${errors[obj]}`)
                    }
                    $('.payment-form').removeClass('d-none');
                }
            });

            $('.close').on('click', function () {
                $('#loading').hide();
                $('.payment-form').removeClass('d-none');
                $('.checkCalc').closest('.modal-footer').removeClass('d-none');
                $('.payment-info').html(``)
                $('.amount').val('');
            })


        });

    </script>
@endpush

