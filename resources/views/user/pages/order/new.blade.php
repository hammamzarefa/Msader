<div id="new-order">
    <div class="swiper-slide order-form">
        <div class="top-collection-item ">
            <div class="water"></div>
            <div class="collection-item-top">
                <ul>
                    <li class="avatar">
                        <strong
                            style="font-size: 20px;margin-inline-start: 10px;"
                            class="text-white">عملية الطلب</strong>
                    </li>
                    <li class="Prev" onclick="swiper.slidePrev()">
                        <i style="color:#fff;" class="fas fa-arrow-right"></i>
                    </li>
                </ul>
            </div>
            <div class="mt-5 collection-item-thumb d-flex align-items-center justify-content-between">
                <h5>السعر</h5>
                <h5 style="color: #fff">$<span class="main-price" data-price="0.1">{{$service->price}}</span></h5>
            </div>
            <form action="" class="create-item-form">
                <div class="form-grp">
                    <input id="number" type="text" placeholder="رقم اللاعب">
                </div>
                <div class="form-grp">
                    <input class="qty" type="number" onkeypress="return isNumberKey(event)" placeholder="الكمية">
                </div>
                <div class="collection-item-bottom">
                    <ul>
                        <li class="m-auto">
                            <button type="submit" class="btn-form-order">
                                <span>أطلب الآن</span>
                            </button>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</div>
