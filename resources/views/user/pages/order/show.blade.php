@extends('user.layouts.app')
@section('title',__('Orders'))
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>


    <div class="area-bg">

        <!-- sell-nfts-area -->
        <div class="category-area">
            <div class="container">
                <form action="{{ route('user.order.search') }}" method="get">
                    <div class="row gy-2">

                        <div class="col-12 col-lg-4 ">
                            <div class="select">
                                <select name="status">
                                    <option value="-1"
                                            @if(@request()->status == '-1') selected @endif>@lang('app.All Status')</option>
                                    <option value="awaiting"
                                            @if(@request()->status == 'awaiting') selected @endif>@lang('app.Awaiting')</option>
                                    <option value="pending"
                                            @if(@request()->status == 'pending') selected @endif>@lang('app.Pending')</option>
                                    <option value="processing"
                                            @if(@request()->status == 'processing') selected @endif>@lang('app.Processing')</option>
                                    <option value="progress"
                                            @if(@request()->status == 'progress') selected @endif>@lang('app.In Progress')</option>
                                    <option value="completed"
                                            @if(@request()->status == 'completed') selected @endif>@lang('app.Completed')</option>
                                    <option value="partial"
                                            @if(@request()->status == 'partial') selected @endif>@lang('app.Partial')</option>
                                    <option value="canceled"
                                            @if(@request()->status == 'canceled') selected @endif>@lang('app.Cancelled')</option>
                                    <option value="refunded"
                                            @if(@request()->status == 'refunded') selected @endif>@lang('app.Refunded')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="sidebar-search">
                                <input type="text" placeholder="Order Id" name="order_id"
                                       value="{{@request()->order_id}}">
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="sidebar-search">
                                <input type="date" class="form-control" name="date_time" id="datepicker">
                            </div>
                        </div>
                        <div class="col-12 col-sm-2">
                            <button type="submit" class="btn"
                                    style="padding-left: 30px;padding-right: 30px;height: 100%;">بحث <i
                                    class="fa fa-search"></i></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <!-- sell-nfts-area-end -->
        <!-- Latest Transaction -->
        <div class="container mt-5">
            <div class="activity-table-wrap">
                <div class="activity-table-nav">
                    <h4 class="">الطلبات</h4>
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-inline-start: 50px;">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="nft-tab" data-bs-toggle="tab"
                                    data-bs-target="#nft" type="button" role="tab" aria-controls="nft"
                                    aria-selected="true">الكل
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="month-tab" data-bs-toggle="tab" data-bs-target="#month"
                                    type="button" role="tab" aria-controls="month" aria-selected="false">آخر
                                شهر
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="nft" role="tabpanel" aria-labelledby="nft-tab">
                        <div class="activity-table-responsive">
                            <table class="table activity-table">
                                <thead>
                                <tr>
                                    <th class="text-center" scope="col">رقم الطلب</th>
                                    <th class="text-center" scope="col">تفاصيل الطلب</th>
                                    <th class="text-center" scope="col">سعر</th>
                                    <th class="text-center" scope="col" class="time">رابط</th>
                                    <th class="text-center" scope="col" class="time">كود</th>
                                    <th class="text-center" scope="col" class="time">تاريخ الطلب</th>
                                    <th class="text-center" scope="col" class="time">حالة الطلب</th>
                                    <th class="text-center" scope="col" class="time">ملاحظات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $key => $order)
                                    <tr>
                                        <td scope="row" class="">
                                            {{$order->id}}
                                        </td>
                                        <td>
                                            <h5>@lang(optional($order->service)->service_title)</h5>
                                            @lang('Link'): @lang($order->link)<br>
                                            @lang('Quantity'): @lang($order->quantity) <br>
                                        </td>
                                        <td>@lang($order->price) @lang(config('basic.currency'))</td>
                                        <td>@lang($order->link ?? 'N/A')
                                            @if($order->service->category->type == 'NUMBER' && !$order->code)
                                                <span><i class="fas fa-sync-alt"
                                                         onclick="checksms({{ $order->id }})"></i></span>
                                            @endif</td>
                                        <td>@lang($order->code )</td>
                                        <td>@lang(dateTime($order->created_at, 'd/m/Y - h:i A' ))</td>
                                        <td @class([
        'fw-bold',
        'text-danger'=>in_array($order->status,['Awaiting','canceled','refunded']) ,
        'text-info'=>in_array($order->status,['pending','processing']) ,
        'text-warning'=>in_array($order->status,['progress','partial']) ,
        'text-success'=>in_array($order->status,['completed']) ,
        ])>{{$order->status}}</td>
                                        <td>
                                            @if(optional($order->service)->service_status == 1)
                                                <button type="button"

                                                        class="btn btn-sm btn-success  orderBtn" data-bs-toggle="modal"
                                                        data-bs-target="#description" id="details"
                                                        data-service_id="{{$order->service_id}}"
                                                        data-servicetitle="{{optional($order->service)->service_title}}"
                                                        data-description="{{optional($order->service)->description}}">
                                                    <i class="fa fa-cart-plus"></i>
                                                </button>
                                            @endif

                                            @if($order->reason)
                                                <button type="button"
                                                        data-reason="{{$order->reason}}"
                                                        class="btn btn-sm btn-info  infoBtn"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#infoModal"><i class="fa fa-info"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {{ $orders->appends($_GET)->links() }}
                        </div>
                    </div>
                    <div class="tab-pane fade" id="month" role="tabpanel" aria-labelledby="month-tab">
                        <div class="activity-table-responsive">
                            <table class="table activity-table">
                                <thead>
                                <tr>
                                    <th class="text-center" scope="col">رقم المعاملة</th>
                                    <th class="text-center" scope="col">الكمية</th>
                                    <th class="text-center" scope="col">الملاحظات</th>
                                    <th class="text-center" scope="col" class="time">التاريخ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td scope="row" class="">
                                        FTZW9MOBGRE1
                                    </td>
                                    <td class="text-success fw-bold" id="increase">
                                        +0.76 USD
                                        <img width="10"
                                             src="{{asset('assets/themes/user-v2/img/icons/title_icon01.png')}}" alt="">
                                    </td>
                                    <td> استرجاع الرصيد بعد تحويل حالة الطلب الى مسترجع</td>
                                    <td> 28 Oct 2023 01:34 PM</td>
                                </tr>
                                <tr>
                                    <td scope="row" class="">
                                        FTZW9MOBGRE1
                                    </td>
                                    <td class="text-danger fw-bold"> -0.76 USD</td>
                                    <td> استرجاع الرصيد بعد تحويل حالة الطلب الى مسترجع</td>
                                    <td> 28 Oct 2023 01:34 PM</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Latest Transaction -->

    </div>

    <div id="infoModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-colored-header">
                    <h5 class="modal-title">@lang('app.Note')</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="info-reason"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn waves-effect waves-light btn-dark"
                            data-bs-dismiss="modal">@lang('app.Close')</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="description">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header modal-colored-header ">
                    <h4 class="modal-title" id="title"></h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                            <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                  d="M5 19L19 4.99996"></path>
                            <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                  d="M19 19L4.99996 4.99996"></path>
                        </svg>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="servicedescription">
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">@lang('app.Close')</button>
                    <a href="" type="submit" class="btn btn-primary order-now">@lang('app.Order Now')</a>
                </div>

            </div>
        </div>
    </div>
@endsection


@push('js')
    <script>
        'use strict';
        $(document).on('click', '.infoBtn', function () {
            var modal = $('#infoModal');
            var id = $(this).data('service_id');
            var orderRoute = "{{route('user.order.create')}}" + '?serviceId=' + id;
            $('.order-now').attr('href', orderRoute);
            modal.find('.info-reason').html($(this).data('reason'));
        });

        $(document).on('click', '#details', function () {
            var title = $(this).data('servicetitle');
            var id = $(this).data('service_id');

            var orderRoute = "{{route('user.order.create')}}" + '?serviceId=' + id;
            $('.order-now').attr('href', orderRoute);

            var description = $(this).data('description');
            $('#title').text(title);
            $('#servicedescription').text(description);
        });
    </script>
    <script>
        function checksms($id) {
            var url = "{{ route('userApiKey') }}";
            let data = {
                "_token": "{{ csrf_token() }}",
                action: 'smscode',
                id: $id,
                api_key: '{{auth()->user()->api_token}}'
            };
            // url = url.replace(':id', $id);
            {{--document.location.href=url;--}}
            $.ajax({
                type: 'post',
                url: url,
                // url : url.replace(':id', $id),
                data: data,
                success: function (data) {
                    if (data.status == 'success') {
                        $('#' + $id).text(data.smsCode)
                    } else {
                        alert('تأكد من طلب الرمز ثم اعد المحاولة')
                    }
                }
            });
        }
    </script>
@endpush
