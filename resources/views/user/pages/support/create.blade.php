@extends('user.layouts.app')
@section('title')
    @lang($page_title)
@endsection
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>

    <div class="area-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12">
                    <form action="{{route('user.ticket.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-6 text-end mb-3">
                                <label for="firstname" class="text-white mb-2">@lang('app.Subject')</label>
                                <input id="firstname" type="text" name="subject"
                                       placeholder="@lang('app.Enter Subject')"
                                       value="{{old('subject')}}">
                                @error('subject')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="col-md-12 col-12 text-end mb-3">
                                <label for="message" class="text-white mb-2">@lang('app.Message')</label>
                                <textarea rows="4" id="message" name="message"
                                          placeholder="@lang('app.Enter Message')"></textarea>
                            </div>

                            <div class="col-md-12 col-12 text-end mb-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">@lang('app.Upload')</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input upload-box" id="upload"
                                               name="attachments[]"
                                               multiple>
                                        <label class="custom-file-label"
                                               for="inputGroupFile01">@lang('app.Choose file')</label>
                                    </div>

                                </div>

                                <p class="text-danger select-files-count"></p>

                                @error('attachments')
                                <div class="error text-danger">@lang($message) </div>
                                @enderror
                            </div>

                            <div class="col-md-12 col-12 text-end mb-3">
                                <button class="btn" style="width: 100%;">
                                    ارسال
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script>
        $(document).ready(function () {
            'use strict';
            $(document).on('change', '#upload', function () {
                var fileCount = $(this)[0].files.length;
                $('.select-files-count').text(fileCount + ' file(s) selected')
            })
        });
    </script>

@endpush
