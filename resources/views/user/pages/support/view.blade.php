@extends('user.layouts.app')
@section('title')
    @lang($page_title)
@endsection
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>


    <div class="container mt-5">
        <div class="activity-table-wrap">
            <div class="activity-table-nav">
                <h4 class="">@lang($page_title)</h4>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="nft" role="tabpanel" aria-labelledby="nft-tab">
                    <div class="activity-table-responsive">
                        <h5 class="card-title m-2 ">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-sm-10">
                                    @if($ticket->status == 0)
                                        <span class="badge badge-pill badge-primary">@lang('app.Open')</span>
                                    @elseif($ticket->status == 1)
                                        <span class="badge badge-pill badge-success">@lang('app.Answered')</span>
                                    @elseif($ticket->status == 2)
                                        <span class="badge badge-pill badge-dark">@lang('app.Customer Reply')</span>
                                    @elseif($ticket->status == 3)
                                        <span class="badge badge-pill badge-danger">@lang('app.Closed')</span>
                                    @endif
                                    [{{trans('Ticket#'). $ticket->ticket }}] {{ $ticket->subject }}
                                </div>
                                <div class="col-sm-2 text-sm-right mt-sm-0 mt-3">

                                    <button type="button" class="btn btn-sm btn-outline-danger btn-rounded"
                                            data-bs-toggle="modal"
                                            data-bs-target="#closeTicketModal"><i
                                            class="fas fa-times-circle"></i> {{trans('Close')}}</button>
                                </div>
                            </div>
                        </h5>
                        <div class="card-body border mx-2">
                            <form class="form-row" action="{{ route('user.ticket.reply', $ticket->id)}}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="col-sm-10 col-12">
                                    <div class="form-group mt-0 mb-0">
                                        <textarea name="message" class=" ticket-box" id="textarea1"
                                                  placeholder="@lang('app.Type Here')"
                                                  rows="3">{{old('message')}}</textarea>
                                    </div>
                                    @error('message')
                                    <span class="text-danger">{{trans($message)}}</span>
                                    @enderror
                                </div>


                                <div class="col-sm-6 col-12">
                                    <div class="justify-content-sm-end justify-content-start mt-sm-0 mt-2 align-items-start d-flex h-100">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">@lang('app.Upload')</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input upload-box" id="upload"
                                                       name="attachments[]"
                                                       multiple>
                                                <label class="custom-file-label"
                                                       for="inputGroupFile01">@lang('app.Choose file')</label>
                                            </div>

                                        </div>

                                        <p class="text-danger select-files-count"></p>

                                        @error('attachments')
                                        <div class="error text-danger">@lang($message) </div>
                                        @enderror


                                    </div>

                                    @error('attachments')
                                    <span class="text-danger">{{trans($message)}}</span>
                                    @enderror
                                </div>

                                <div class="col-sm-12 col-12">
                                    <button type="submit"
                                            class="btn btn-circle btn-lg btn-success float-right text-white"
                                            title="{{trans('app.Reply')}}" name="replayTicket"
                                            value="1"><i class="fas fa-paper-plane"></i></button>
                                </div>
                            </form>
                        </div>

                        @if(count($ticket->messages) > 0)
                            <div class="chat-box scrollable position-relative scroll-height">
                                <ul class="chat-list list-style-none ">
                                    @foreach($ticket->messages as $item)
                                        @if($item->admin_id == null)
                                            <li class="chat-item list-style-none replied mt-3 text-right">
                                                <div class="chat-img d-inline-block">

                                                    <img
                                                        src="{{getFile(config('location.user.path').optional($ticket->user)->image)}}"
                                                        alt="user"
                                                        class="rounded-circle" width="45">
                                                </div>
                                                <div class="w-100">
                                                    <div class="chat-content d-inline-block pr-3">
                                                        <h6 class="font-weight-medium">{{optional($ticket->user)->username}} </h6>

                                                        <div class="media flex-row-reverse">

                                                            <div class="msg p-2 d-inline-block mb-1">
                                                                {{$item->message}}
                                                            </div>

                                                        </div>

                                                        @if(0 < count($item->attachments))
                                                            <div class="d-flex justify-content-end">
                                                                @foreach($item->attachments as $k=> $image)
                                                                    <a href="{{route('user.ticket.download',encrypt($image->id))}}"
                                                                       class="ml-3 nowrap "><i
                                                                            class="fa fa-file"></i> @lang('app.File') {{++$k}}
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        @endif


                                                    </div>
                                                    <div
                                                        class="chat-time d-block font-10 mt-0 mr-0 mb-3">{{dateTime($item->created_at, 'd M, y h:i A')}}
                                                    </div>
                                                </div>

                                            </li>
                                        @else
                                            <li class="chat-item list-style-none mt-3">
                                                <div class="chat-img d-inline-block">
                                                    <img
                                                        src="{{getFile(config('location.admin.path').optional($item->admin)->image)}}"
                                                        alt="user"
                                                        class="rounded-circle" width="45">
                                                </div>
                                                <div class="chat-content d-inline-block pl-3">
                                                    <h6 class="font-weight-medium">{{optional($item->admin)->name}}</h6>

                                                    <div class="media">
                                                        <div class="msg p-2 d-inline-block mb-1">
                                                            {{$item->message}}
                                                        </div>

                                                    </div>


                                                    @if(0 < count($item->attachments))
                                                        <div class="d-flex justify-content-start">
                                                            @foreach($item->attachments as $k=> $image)
                                                                <a href="{{route('user.ticket.download',encrypt($image->id))}}"
                                                                   class="mr-3 nowrap"><i
                                                                        class="fa fa-file"></i> @lang('app.File') {{++$k}}
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    @endif


                                                </div>
                                                <div
                                                    class="chat-time d-block font-10 mt-0 mr-0 mb-3">{{dateTime($item->created_at, 'd M, y h:i A')}}</div>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>




    <div class="modal fade" id="closeTicketModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-bg">

                <form method="post" action="{{ route('user.ticket.reply', $ticket->id) }}">
                    @csrf
                    @method('PUT')

                    <div class="modal-header modal-colored-header ">
                        <h5 class="modal-title"> @lang('Confirmation')!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                      d="M5 19L19 4.99996"></path>
                                <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                      d="M19 19L4.99996 4.99996"></path>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>@lang('Are you want to close ticket?')</p>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">
                            @lang('Close')
                        </button>

                        <button type="submit" class="btn btn-primary" name="replayTicket"
                                value="2">@lang("Confirm")
                        </button>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection


@push('js')
    <script>
        'use strict';
        $(document).on('change', '#upload', function () {
            var fileCount = $(this)[0].files.length;
            $('.select-files-count').text(fileCount + ' file(s) selected')
        })
    </script>
@endpush


