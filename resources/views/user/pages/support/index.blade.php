@extends('user.layouts.app')
@section('title')
    @lang($page_title)
@endsection
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>

    <div class="container mt-5">
        <div class="activity-table-wrap">
            <div class="activity-table-nav">
                <h4 class="">تذاكر</h4>

            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="nft" role="tabpanel" aria-labelledby="nft-tab">
                    <div class="activity-table-responsive">
                        <table class="table activity-table">
                            <thead>
                            <tr>
                                <th scope="col">@lang('app.Subject')</th>
                                <th scope="col">@lang('app.Status')</th>
                                <th scope="col">@lang('app.Last Reply')</th>
                                <th scope="col">@lang('app.Action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($tickets as $key => $ticket)
                                <tr>
                                    <td data-label="@lang('Subject')">
                                        <span
                                            class="font-weight-bold"> [{{ trans('Ticket#').$ticket->ticket }}] {{ $ticket->subject }} </span>
                                    </td>
                                    <td data-label="@lang('Status')">
                                        @if($ticket->status == 0)
                                            <span class="badge badge-pill badge-success">@lang('Open')</span>
                                        @elseif($ticket->status == 1)
                                            <span class="badge badge-pill badge-primary">@lang('Answered')</span>
                                        @elseif($ticket->status == 2)
                                            <span class="badge badge-pill badge-warning">@lang('Replied')</span>
                                        @elseif($ticket->status == 3)
                                            <span class="badge badge-pill badge-dark">@lang('Closed')</span>
                                        @endif
                                    </td>

                                    <td data-label="@lang('Last Reply')">
                                        {{diffForHumans($ticket->last_reply) }}
                                    </td>

                                    <td data-label="@lang('Action')">
                                        <a href="{{ route('user.ticket.view', $ticket->ticket) }}"
                                           class="btn btn-sm btn-outline-info" title="Details">
                                            <i class="fa fa-eye mr-1"></i>@lang('Details')
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="100%">
                                        <p class="text-dark">@lang('app.No Data Found!')</p>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $tickets->appends($_GET)->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
