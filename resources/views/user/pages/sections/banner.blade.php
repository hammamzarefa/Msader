<section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">
    @if($banner)
        <div class="container">
            <div class="swiper0">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper ">
                    {{--                @foreach($banners as $banner)--}}
                    <!-- Slides -->
                    <div class="swiper-slide d-flex">
                        <div class="col-5 col-md-6 text-center">
                            <img class="breadcrumb-img"
                                 src="{{getFile(config('location.content.path').@$banner->content->contentMedia?->description->image)}}"
                                 alt="">
                        </div>
                        <div class="col-7 col-md-6 text-end">
                            <div class="breadcrumb-content">
                                <h3 class="title">{{$banner?->description->title}}</h3>
                                <p class="mt-1">{!! $banner?->description->short_description !!}</p>
                                <a class="breadcrumb-button" href="{{$banner?->description->button_url}}"
                                   style="--color: #d300ff;">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    {{$banner?->description->button_name}}
                                </a>
                            </div>
                        </div>
                    </div>
                    {{--                @endforeach--}}
                </div>
            </div>
        </div>
    @endif
</section>
