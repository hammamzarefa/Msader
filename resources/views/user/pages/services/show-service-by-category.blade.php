
@if(count($services))
    <div id="show-orders">
        <section class="inner-explore-products content">
            <div class="container">
                <div class="row justify-content-center services">
                    @foreach($services as $service)
                        <div class="col-xl-3 col-lg-4 col-sm-6 orders-item" id="order-{{ $loop->index }}">
                            <div class="swiper-{{ $loop->index }}">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="top-collection-item">
                                            <div class="collection-item-top">
                                                <ul>
                                                    <li class="avatar"></li>
                                                </ul>
                                            </div>
                                            <div class="collection-item-thumb">
                                                <img
                                                        src="{{$service->image ? getFile(config('location.service.path').$service->image) : getFile(config('location.category.path').$service->category->image) }}"
                                                        alt="">
                                            </div>
                                            <div class="collection-item-content">
                                                <h5 class="title">
                                                    <a> {{$service->service_title}} </a>

                                                    <span class="price"
                                                          data-value="{{$service->user_rate ?? $service->price}}">@lang(config('basic.currency_symbol')){{$service->user_rate ?? $service->price}}</span>
                                                </h5>
                                                <hr>
                                                <h5 class="title">
                                                    <a>Max : <span
                                                                class="Max m-0">{{$service->max_amount}}</span></a>
                                                    <a>Min : <span
                                                                class="Min m-0">{{$service->min_amount}}</span></a>
                                                </h5>
                                            </div>
                                            <div class="collection-item-bottom">
                                                <ul>
                                                    <li
                                                        onclick="slideNext( {{ $loop->index }},{{$service->id}})"
                                                        class="bid m-auto btn">
                                                        أطلب الان
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide order-form" >
                                        <div class="top-collection-item ">
                                            <div class="water"></div>
                                            <div class="collection-item-top">
                                                <ul>
                                                    <li class="avatar">
                                                        <strong
                                                                style="font-size: 20px;margin-inline-start: 10px;"
                                                                class="text-white">عملية الطلب</strong>
                                                    </li>
                                                    <li class="Prev p-2" onclick="slidePrev({{ $loop->index }})">
                                                        <i style="color:#fff;" class="fas fa-arrow-right"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="target-{{ $loop->index }}"
                                                    class="mt-5 collection-item-thumb d-flex align-items-center justify-content-between">
                                                <h5>السعر</h5>
                                                <h5 style="color: #fff">$<span class="main-price"
                                                id="main-price-{{ $loop->index }}"
                                                data-price="{{$service->user_rate ?? $service->price}}">{{$service->user_rate ?? $service->price}}</span>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endif

<script>
    "use strict"
    var loop_index = 0;

    var swipers = [];

    // Initialize Swiper instances dynamically
    @if(count($services))
        @foreach($services as $service)
            swipers.push(new Swiper('.swiper-{{ $loop->index }}', {
            loop: false,
            speed: 1000,
            effect: "cube",
            simulateTouch: false
        }));
        @endforeach
    @endif
    // Function to slide to the next slide for a specific swiper index
    function slideNext(index,id) {
        loop_index = index;
        if (swipers[index]) {
            swipers[index].slideNext();
        }
        var items = document.querySelectorAll('.orders-item');
        items.forEach( item => {
                if(item.getAttribute("id") == `order-${index}`){
                    setTimeout(() => {
                        item.className = `col-md-6 col-12 orders-item`;
                    }, 1000);
                }
                else{
                    item.classList.add(`anime-hide`);
                    setTimeout(() => {
                        item.classList.add(`d-none`);
                    }, 1000);

                }
            }
            );
       var target = document.getElementById(`target-${index}`);
       var categoryType = '{{$service->category->type}}';
       var defaultQuantity = categoryType == 'SMM' ? 1000 : 1;
       var priceUnit = categoryType == 'SMM' ? 1000 : 1;
       var price = ({{$service->user_rate ?? $service->price}} * defaultQuantity) / priceUnit;

       var formHtml = `
       <form action="{{route('user.order.store')}}" class="create-item-form" id="form-${index}" method="POST">
       @csrf
                                                <input name="service" value="${id}" hidden>
                                                 <input name="category" value="{{$service->category_id}}" hidden>
                                                @if(!in_array($service->category->type,['NUMBER,CODE']) )
                                                    <div class="form-grp">
                                                        <input id="number" name="link" type="text"
                                                               placeholder="{{getLinkInputPlacholder($service->category->type)}}">
                                                    </div>
                                                    @endif
                                                <div class="form-grp">
                                                    <input class="qty"
                                                    id="qty-${index}" name="quantity" type="number" value="${defaultQuantity}"
                                                    oninput= "setQty(${index}, ${priceUnit})"
                                                    onkeypress="return isNumberKey(event)"
                                                           placeholder="الكمية">
                                                </div>
                                                <div class="form-grp check-grp">
                                                <input onclick="checkOrder()" class="check" id="check" name="check" type="checkbox">
                                                <label for="check">يرجى التأكيد قبل الطلب</label>
                                                </div>
                                                <div class="collection-item-bottom">
                                                    <ul>
                                                        <li class="m-auto">
                                                            <button disabled type="submit" class="btn-form-order" onclick="Order()">
                                                                <span>أطلب الآن</span>
                                                            </button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </form>
       `;
       target.insertAdjacentHTML('afterend', formHtml );

    }

    function Order(){
        event.preventDefault();
        var checkInput = document.getElementById("check").checked;
        if(checkInput){
            $('.water').addClass('active');
		setTimeout(() => {
            document.querySelector('.create-item-form').submit();
		}, 5000);
        }
    }
    function checkOrder() {
        var checkInput = document.querySelector('#check').checked;
        if (checkInput) {
            document.querySelector('.btn-form-order').removeAttribute('disabled');
        } else {
            document.querySelector('.btn-form-order').setAttribute("disabled", "");


        }
    }
    function setQty(index, priceUnit) {
        var qty = document.getElementById(`qty-${index}`).value;
        var oldPrice = document.getElementById(`main-price-${index}`).dataset.price;
        var newPrice = (qty * oldPrice) / priceUnit;
        document.getElementById(`main-price-${index}`).innerHTML = newPrice.toFixed(2);
    }
    function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : evt.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		  return false;
		return true;
	  }


    function slidePrev(index) {
        loop_index = 0;
        if (swipers[index]) {
            swipers[index].slidePrev();
        }
        var dataprice = document.getElementById(`main-price-${index}`).dataset.price;
        document.getElementById(`main-price-${index}`).innerHTML = dataprice;
        document.getElementById(`form-${index}`).remove();
        var items = document.querySelectorAll('.orders-item');
        items.forEach( item => {
            if(item.getAttribute("id") == `order-${index}`){
                        item.className =`col-xl-3 col-lg-4 col-sm-6 orders-item`;
                }
                else{
                    item.classList.remove(`anime-hide`);
                    setTimeout(() => {
                        item.classList.remove(`d-none`);
                        item.classList.add(`anime-show`);
                    }, 1000);

                }
            }
            );


    }

</script>
