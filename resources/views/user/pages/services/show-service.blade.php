@extends('user.layouts.app')
@section('title')
    @lang('Service')
@endsection
@section('content')

    <!-- header-area-end -->
   @include('user.pages.sections.banner')
    <!-- main-area -->
    <div class="area-bg" style="overflow: hidden;">
        <div class="d-flex justify-content-center" style="overflow: hidden;">
            <div id="btn-hide-order" onclick="back()" class="btn" style="padding: 15px 40px;">رجوع</div>
        </div>
        <div id="show-products" class="show">
            <section class="inner-explore-products content">
                <div class="container">
                    <div class="row justify-content-center mt-5">
                        @foreach($categories as $category)
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card-game btn-order" onclick="show({{$category->id}})">
                                    <div class="wrapper">
                                        <img
                                            src="{{ getFile(config('location.category.path').$category->image) }}"
                                            alt="{{$category->category_title}}"
                                            class="cover-image"/>
                                    </div>
                                    <h4 class="text-center title">{{$category->category_title}}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
        <div id="services">
        </div>
    </div>

    @push('js')
        <script>
            function back() {
                $('#btn-hide-order').removeClass("show");
                $("#show-orders").remove();
                $("#show-products").show(500);
            }

            function show(id) {
                $('#btn-hide-order').addClass("show");
                $("#show-products").hide(500);
                // Livewire.emit('categorySelected', {'c_id': id})
                // $("#show-products").hide(500);
                $.ajax({
                    url: '/user/services/' + id,
                    method: 'GET',
                    success: function (response) {
                        // Update products container with fetched products
                        $('#services').html(response);
                    },
                    error: function (xhr, status, error) {
                        console.error(error);
                    }
                });

            }
        </script>
    @endpush

@endsection









