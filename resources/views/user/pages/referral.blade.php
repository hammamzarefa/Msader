@extends('user.layouts.app')
@section('title')
    @lang('Dashboard')
@endsection
@section('content')

    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>
    <div class="category-area">
        <div class="container">

            <div class="row">
                <form>
                    <div class="form-group form-block mb-50 pr-15 pl-15">
                        <h5 class="mb-15">@lang('app.Referral Link')</h5>
                        <div class="input-group mb-50">

                            <input readonly
                                   type="text"
                                   placeholder="@lang('app.Search for Transaction ID')"
                                   name="transaction_id"
                                   id="sponsorURL"
                                   value="{{route('register.sponsor',[Auth::user()->username])}}">


                            <div class="input-group-append">
                                <button type="button" class="input-group-text copytext" id="copyBoard"
                                        onclick="copyFunction(event)">
                                    <i class="fa fa-copy"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>


    <div class="container mt-5 mb-2">
        <div class="activity-table-wrap">
            <div class="activity-table-nav">
                {{--                <h4 class="">الطلبات</h4>--}}
                <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-inline-start: 50px;">
                    @foreach($referrals as $key => $referral)
                        <li class="nav-item " role="presentation">
                            <button class="nav-link @if($key == '1')  active  @endif"
                                    id="v-pills-{{$key}}-tab" data-bs-toggle="tab"
                                    data-bs-target="#v-pills-{{$key}}"
                                    type="button" role="tab" aria-controls="nft"
                                    aria-selected="true">@lang('Level') {{$key}}
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                @foreach($referrals as $key => $referral)
                    <div class="tab-pane fade @if($key == '1') show active  @endif " id="v-pills-{{$key}}"
                         role="tabpanel" aria-labelledby="nft-tab">
                        <div class="activity-table-responsive">
                            <table class="table activity-table">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('Name')</th>
                                    <th scope="col">@lang('Email')</th>
                                    <th scope="col">@lang('Phone Number')</th>
                                    <th scope="col">@lang('Joined At')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($referral as $user)
                                    <tr>

                                        <td data-label="@lang('Name')">{{$user->username}}</td>
                                        <td data-label="@lang('Email')">{{$user->email}}</td>
                                        <td data-label="@lang('Phone Number')">
                                            {{$user->mobile}}
                                        </td>
                                        <td data-label="@lang('Joined At')">
                                            {{dateTime($user->created_at)}}
                                        </td>

                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="100%">{{trans('No Data Found!')}}</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
{{--                            {{ $referral->appends($_GET)->links() }}--}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


@endsection
@push('js')
    <script>
        "use strict";

        function copyFunction(e) {
            e.preventDefault();
            console.log('sd');
            var copyText = document.getElementById("sponsorURL");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            /*For mobile devices*/
            document.execCommand("copy");
            Notiflix.Notify.Success(`Copied: ${copyText.value}`);
        }
    </script>
@endpush
