@extends('user.layouts.app')

@section('content')
    <div class="area-bg" style="padding: 50px 0; margin-top: 130px;">
        <div class="container">


            <div class="row">
                <div class="col-md-4 col-12">
                    <form method="post" action="{{ route('user.updateProfile') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-profile">
                            <label for="formFile" class="form-label">
                                <img style="width: 100%;" src="{{getFile(config('location.user.path').$user->image)}}"
                                     alt="">
                            </label>
                            <input class="form-control d-none" type="file" id="formFile" name="image">
                            <p class="text-center text-white">@lang('app.Joined At') @lang($user->created_at->format('d M, Y g:i A'))</p>
                            <button class="btn">تعديل الصورة الشخصية</button>
                            @error('image')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </form>
                </div>
                <div class="col-md-8 col-12">
                    <div class="card-profile">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="flex-wrap: nowrap">
                            <li class="nav-item" role="presentation">
                                <button
                                    class="nav-link {{ $errors->has('profile') ? 'active' : ($errors->has('password') ? '' : 'active') }}"
                                    id="Information-tab" data-bs-toggle="tab"
                                    data-bs-target="#Information" type="button" role="tab"
                                    aria-controls="Information" aria-selected="true">معلومات الملف الشخصي
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link {{ $errors->has('password') ? 'active' : '' }}"
                                        id="Password-tab" data-bs-toggle="tab"
                                        data-bs-target="#Password" type="button" role="tab" aria-controls="Password"
                                        aria-selected="false">إعداد كلمة المرور
                                </button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div
                                class="tab-pane fade  {{ $errors->has('profile') ? 'show active' : ($errors->has('password') ? '' : ' show active') }}"
                                id="Information" role="tabpanel"
                                aria-labelledby="Information-tab">
                                <div class="profile-form m-0 mt-3" style="width: 100%;">
                                    <form method="post" action="{{ route('user.updateInformation') }}"
                                          enctype="multipart/form-data" id="profile-info">
                                        @method('put')
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6 col-6 text-end mb-3">
                                                <label for="firstname" class="text-white mb-2">الاسم الاول</label>
                                                <input id="firstname" type="text" name="firstname"
                                                       placeholder="الاسم الاول"
                                                       value="{{old('firstname')?: $user->firstname }}">
                                                @error('firstname')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6 col-6 text-end mb-3">
                                                <label for="lastname" class="text-white mb-2">الاسم الاخير</label>
                                                <input id="lastname" name="lastname" type="text"
                                                       placeholder="الاسم الاخير"
                                                       value="{{old('lastname')?: $user->lastname }}">
                                                @error('lastname')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6 col-6 text-end mb-3">
                                                <label for="username" class="text-white mb-2">اسم المستخدم</label>
                                                <input id="username" name="username" type="text"
                                                       placeholder="اسم المستخدم"
                                                       value="{{old('username')?: $user->username }}">
                                                @error('username')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6 col-6 text-end mb-3">
                                                <label for="email" class="text-white mb-2">عنوان البريد
                                                    الإلكتروني</label>
                                                <input id="email" type="email" name="email"
                                                       placeholder="عنوان البريد الإلكتروني"
                                                       value="{{ $user->email }}"
                                                       readonly>
                                            </div>
                                            <div class="col-md-6 col-6 text-end mb-3">
                                                <label for="phone" class="text-white mb-2">رقم التليفون</label>
                                                <input id="phone" name="phone" type="tel" placeholder="رقم التليفون"
                                                       readonly
                                                       value="{{$user->phone}}">
                                            </div>

                                            @if(0 < count($languages))
                                                <div class="col-md-6 col-6 text-end mb-3">
                                                    <label for="" class="text-white mb-2">اللغة المفضلة</label>
                                                    <select name="language_id"
                                                            class="form-select" aria-label="Default select example"
                                                            style="background-color: #131129;color: #fff;height: 47px;">
                                                        <option selected>اللغة المفضلة</option>
                                                        @foreach($languages as $item)
                                                            <option value="{{$item->id}}"
                                                                    @if($item->id == $user->language_id) selected @endif>{{$item->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            @endif


                                            <div class="col-md-12 col-12 text-end mb-3">
                                                <label for="address" class="text-white mb-2">عنوان</label>
                                                <textarea id="address" name="address">{{$user->address}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-12 text-end mb-3">
                                            <button class="btn" style="width: 100%;">
                                                تحديث المستخدم
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <div class="tab-pane fade {{ $errors->has('password') ? 'show active' : '' }}" id="Password"
                                 role="tabpanel" aria-labelledby="Password-tab">
                                <div class="profile-form m-0 mt-3" style="width: 100%;">
                                    <form method="post" action="{{ route('user.updatePassword') }}">
                                        @csrf
                                        <div class="col-md-12 col-12 text-end mb-3">
                                            <label for="current_password" class="text-white mb-2">كلمة السر
                                                الحالية</label>
                                            <input id="current_password" type="password" placeholder="كلمة السر الحالية"
                                                   name="current_password">
                                            @if($errors->has('current_password'))
                                                <div
                                                    class="error text-danger">@lang($errors->first('current_password')) </div>
                                            @endif
                                        </div>
                                        <div class="col-md-12 col-12 text-end mb-3">
                                            <label for="password" class="text-white mb-2">كلمة السر الجديدة</label>
                                            <input id="password" type="password" placeholder="كلمة السر الجديدة"
                                                   name="password">
                                            @if($errors->has('password'))
                                                <div
                                                    class="error text-danger">@lang($errors->first('password')) </div>
                                            @endif
                                        </div>
                                        <div class="col-md-12 col-12 text-end mb-3">
                                            <label for="password_confirmation" class="text-white mb-2">تأكيد كلمة
                                                المرور</label>
                                            <input id="password_confirmation" type="password"
                                                   placeholder="تأكيد كلمة المرور" name="password_confirmation">
                                            @if($errors->has('password_confirmation'))
                                                <div
                                                    class="error text-danger">@lang($errors->first('password_confirmation')) </div>
                                            @endif
                                        </div>
                                        <div class="col-md-12 col-12 text-end mb-3">
                                            <button class="btn" style="width: 100%;">
                                                تحديث كلمة السر
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="profile-form">
                            <form>
                                <div class="form-group form-block mb-50 pr-15 pl-15">
                                    <h5 class="mb-15">@lang('app.Referral Link')</h5>
                                    <div class="input-group mb-50">
                                        <input type="text"
                                               value="{{route('register.sponsor',[Auth::user()->username])}}"
                                               class="form-control form-control-lg bg-transparent" id="sponsorURL"
                                               readonly>
                                        <div class="input-group-append">
                                            <span class="input-group-text copytext" id="copyBoard"
                                                  onclick="copyFunction()">
                                                <i class="fa fa-copy"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>


                </div>

            </div>

        </div>
    </div>
@endsection
@push('js')
    <script>
        "use strict";
        $(document).on('click', '#image-label', function () {
            $('#image').trigger('click');
        });
        $(document).on('change', '#image', function () {
            var _this = $(this);
            var newimage = new FileReader();
            newimage.readAsDataURL(this.files[0]);
            newimage.onload = function (e) {
                $('#image_preview_container').attr('src', e.target.result);
            }
        });


        function copyFunction() {
            var copyText = document.getElementById("sponsorURL");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            /*For mobile devices*/
            document.execCommand("copy");
            Notiflix.Notify.Success(`Copied: ${copyText.value}`);
        }
    </script>
@endpush

