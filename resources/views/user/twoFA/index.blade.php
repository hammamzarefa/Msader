@extends('user.layouts.app')
@section('title',trans('app.2FA Security'))

@section('content')
    <section class="breadcrumb-area breadcrumb-bg" style="overflow: hidden;">

    </section>

    <div class="area-bg pt-4 [m]b-4">
        <div class="container">


            <div class="row">

                @if(auth()->user()->two_fa)
                    <div class="col-lg-6 col-md-6 mb-3">
                        <div class="card card-type-1 text-center shadow-sm  card-profile">
                            <div class="card-body">
                                <h2 class="card-title py-3 mb-4 greenColorText font-weight-bold">@lang('app.Two Factor Authenticator')</h2>
                                <div class="form-group form-block">
                                    <div class="input-group">
                                        <input type="text" value="{{$previousCode}}"
                                               class="form-control form-control-lg bg-transparent" id="referralURL"
                                               readonly>
                                        <div class="input-group-append" title="Copy">
                                    <span class="input-group-text copytext" id="copyBoard"
                                          onclick="copyFunction()">
                                        <i class="fa fa-copy"></i>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mx-auto text-center">
                                    <img class="mx-auto" src="{{$previousQR}}">
                                </div>

                                <div class="form-group mx-auto text-center">
                                    <a href="javascript:void(0)" class="btn btn-block btn-lg btn-danger"
                                       data-toggle="modal"
                                       data-target="#disableModal">@lang('app.Disable Two Factor Authenticator')</a>
                                </div>
                            </div>

                        </div>
                    </div>
                @else
                    <div class="col-lg-6 col-md-6 mb-3">
                        <div class="card card-type-1 text-center box-shadow  card-profile">
                            <div class="card-body">
                                <h2 class="card-title py-3 mb-4 greenColorText font-weight-bold">@lang('app.Two Factor Authenticator')</h2>

                                <div class="form-group ">
                                    <div class="input-group">
                                        <input type="text" value="{{$secret}}"
                                               class="form-control form-control-lg bg-transparent" id="referralURL"
                                               readonly>
                                        <div class="input-group-append" title="copy">
                                        <span class="input-group-text copytext" id="copyBoard"
                                              onclick="copyFunction()">
                                            <i class="fa fa-copy"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mx-auto text-center">
                                    <img class="mx-auto" src="{{$qrCodeUrl}}">
                                </div>

                                <div class="form-group mx-auto text-center">
                                    <a href="javascript:void(0)" class="btn btn-block btn-lg btn-success"
                                       data-bs-toggle="modal"
                                       data-bs-target="#enableModal">@lang('app.Enable Two Factor Authenticator')</a>
                                </div>
                            </div>

                        </div>
                    </div>

                @endif


                <div class="col-md-6 col-12 mb-3">
                    <div class="card card-type-1 text-center box-shadow card-profile">
                        <div class="card-body">
                            <h2 class="card-title py-3 mb-4 greenColorText font-weight-bold">@lang('app.Google Authenticator')</h2>
                            <h6 class="text-uppercase my-3 font16">@lang('app.Use Google Authenticator to Scan the QR code  or use the code')</h6>

                            <p class="p-5 font14">@lang('app.Google Authenticator is a multifactor app for mobile devices. It generates timed codes used during the 2-step verification process. To use Google Authenticator, install the Google Authenticator application on your mobile device.')</p>
                            <a class="btn btn-success btn-md mt-3"
                               href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en"
                               target="_blank">@lang('app.DOWNLOAD APP')</a>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--Enable Modal -->
    <div id="enableModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content form-block">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('app.Verify Your OTP')</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                            <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                  d="M5 19L19 4.99996"></path>
                            <path stroke="currentColor" stroke-linecap="round" stroke-width="2.125"
                                  d="M19 19L4.99996 4.99996"></path>
                        </svg>
                    </button>

                </div>
                <form action="{{route('user.twoStepEnable')}}" method="POST">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="hidden" name="key" value="{{$secret}}">
                            <input type="text" class="form-control bg-transparent" name="code"
                                   placeholder="@lang('app.Enter Google Authenticator Code')">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('app.Close')</button>
                        <button type="submit" class="btn btn-primary">@lang('app.Verify')</button>
                    </div>

                </form>
            </div>

        </div>
    </div>

    <!--Disable Modal -->
    <div id="disableModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content form-block">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('app.Verify Your OTP to Disable')</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{route('user.twoStepDisable')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control bg-transparent" name="code"
                                   placeholder="@lang('app.Enter Google Authenticator Code')">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('app.Close')</button>
                        <button type="submit" class="btn btn-primary">@lang('app.Verify')</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection



@push('js')
    <script>
        function copyFunction() {
            var copyText = document.getElementById("referralURL");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            /*For mobile devices*/
            document.execCommand("copy");
            Notiflix.Notify.Success(`Copied: ${copyText.value}`);
        }
    </script>
@endpush

