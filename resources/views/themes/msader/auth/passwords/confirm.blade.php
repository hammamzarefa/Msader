@extends('themes.msader.layouts.guest')
@section('content')

    <!-- ticker-wrap-->
    <div class="area-bg" style="padding: 50px 0;">
        <div class="box m-auto">
            <div class="login">
                <form method="POST" action="{{route('password.update')}}" class="loginBx">
                    @csrf
                    <h3>
                        <i class="fi-sr-sign-in-alt"></i> @lang('Reset Password')

                    </h3>
                    @if (session('status'))
                        <p class="text-danger"> {{ trans(session('status')) }}</p>
                    @endif
                    @error('token')<p class="text-danger">@lang($message)</p>@enderror
                    @error('email')<p class="text-danger ">@lang($message)</p>@enderror
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="email" value="{{ $email }}">


                    <input type="password" placeholder="@lang('New Password')" name="password">

                    @error('password')
                    <p class="text-danger mt-1">@lang($message)</p>
                    @enderror

                    <input type="password" placeholder="@lang('Confirm Password')" name="password_confirmation">


                    <input type="submit" value="@lang('Reset Password')">


                    @if(basicControl()->reCaptcha_status_login)
                        <div class="col-12 mb-2 input-box">
                            {!! NoCaptcha::renderJs(session()->get('trans')) !!}
                            {!! NoCaptcha::display($basic->theme == 'msader' ? ['data-theme' => 'msader'] : []) !!}
                            @error('g-recaptcha-response')
                            <span class="text-danger mt-1">@lang($message)</span>
                            @enderror
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection
