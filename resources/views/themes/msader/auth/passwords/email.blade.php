@extends('themes.msader.layouts.guest')
@section('content')

    <!-- ticker-wrap-->
    <div class="area-bg" style="padding: 50px 0;">
        <div class="box m-auto">
            <div class="login">
                <form method="POST" action="{{ route('password.email') }}" class="loginBx">
                    @csrf
                    <h3>
                        <i class="fi-sr-sign-in-alt"></i> @lang('Reset Password')
                    </h3>
                    @error('email')<p class="text-danger ">@lang($message)</p>@enderror

                    <input type="email" placeholder="@lang('Enter Your Email Address')" name="email"
                           value="{{old('email')}}">


                    <input type="submit" value="@lang('Send Password Reset Link')">
                    <div class="group">

                        <a href="{{route('register')}}">@lang('register')</a>
                    </div>

                    @if(basicControl()->reCaptcha_status_login)
                        <div class="col-12 mb-2 input-box">
                            {!! NoCaptcha::renderJs(session()->get('trans')) !!}
                            {!! NoCaptcha::display($basic->theme == 'msader' ? ['data-theme' => 'msader'] : []) !!}
                            @error('g-recaptcha-response')
                            <span class="text-danger mt-1">@lang($message)</span>
                            @enderror
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection
