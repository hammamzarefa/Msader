@extends('themes.msader.layouts.guest')
@section('content')

    <!-- ticker-wrap-->
    <div class="area-bg" style="padding: 50px 0;">
        <div class="box m-auto">
            <div class="login">
                <form method="POST" action="{{ route('login') }}" class="loginBx">
                    @csrf
                    <h3>
                        <i class="fi-sr-sign-in-alt"></i> @lang('login')
                        {{--                        <i class="fi-sr-heart"></i>--}}
                    </h3>
                    @error('username')<p class="text-danger">@lang($message)</p>@enderror
                    @error('email')<p class="text-danger ">@lang($message)</p>@enderror

                    <input type="text" placeholder="@lang('username')" name="username" value="{{old('username')}}">


                    <input type="password" placeholder="@lang('password')" name="password" id="">
                    @error('password')
                    <p class="text-danger mt-1">@lang($message)</p>
                    @enderror

                    <input type="submit" value="@lang('login')">
                    <div class="group">
                        <a href="{{ route('password.request') }}">@lang('reset password')</a>
                        <a href="{{route('register')}}">@lang('register')</a>
                    </div>

                    @if(basicControl()->reCaptcha_status_login)
                        <div class="col-12 mb-2 input-box">
                            {!! NoCaptcha::renderJs(session()->get('trans')) !!}
                            {!! NoCaptcha::display($basic->theme == 'msader' ? ['data-theme' => 'msader'] : []) !!}
                            @error('g-recaptcha-response')
                            <span class="text-danger mt-1">@lang($message)</span>
                            @enderror
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection
