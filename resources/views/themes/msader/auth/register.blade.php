@extends('themes.msader.layouts.guest')
@section('content')

    <!-- ticker-wrap-->
    <div class="area-bg" style="padding: 50px 0;">
        <div class="box m-auto" style="height: 1000px; width: 70vw;">
            <div class="login">
                <form method="POST" action="{{ route('register') }}" class="loginBx">
                    @csrf
                    <h2>
                        <i class="fi-sr-sign-in-alt"></i>@lang('register')
                        <i class="fi-sr-heart"></i>
                    </h2>
                    @error('username')<p class="text-danger">@lang($message)</p>@enderror
                    @error('email')<p class="text-danger ">@lang($message)</p>@enderror

{{--                    <input type="text" placeholder="{{__('app.Sponsor By')}}" name="sponsor"--}}
{{--                           value="{{session()->get('sponsor')}}" readonly>--}}

                    <input type="text" placeholder="@lang('firstname')" name="firstname"
                           value="{{old('username')}}">
                    @error('firstname')
                    <p class="text-danger">@lang($message)</p>
                    @enderror

                    <input type="text" placeholder="@lang('lastname')" name="lastname" value="{{old('lastname')}}">
                    @error('lastname')
                    <p class="text-danger">@lang($message)</p>
                    @enderror
                    <input type="text" placeholder="@lang('username')" name="username" value="{{old('username')}}">
                    @error('username')
                    <p class="text-danger">@lang($message)</p>
                    @enderror

                    <input type="email" placeholder="@lang('email')" name="email" value="{{old('email')}}">
                    @error('email')
                    <p class="text-danger">@lang($message)</p>
                    @enderror

                    <div class="input-box col-12">
                        <div class="input-group">

                            <div class="input-group-prepend ">
                                <div class="select">
                                    <select name="phone_code" class=" country_code">
                                        @foreach($countries as $value)
                                            <option value="{{$value['phone_code']}}"
                                                    data-name="{{$value['name']}}"
                                                    data-code="{{$value['code']}}"
                                                {{$country_code == $value['code'] ? 'selected' : ''}}
                                            > {{$value['phone_code']}} <strong>({{$value['name']}})</strong>
                                            </option>
                                        @endforeach
                                    </select>
                                </div>


                            </div>
                            <input type="text" name="phone" class="form-control ps-3 phoneField"
                                   value="{{old('phone')}}" placeholder="@lang('Your Phone Number')">
                        </div>

                        @error('phone')
                        <p class="text-danger mt-1">@lang($message)</p>
                        @enderror
                    </div>

                    <input type="password" placeholder="@lang('password')" name="password" id="password">
                    @error('password')
                    <p class="text-danger mt-1">@lang($message)</p>
                    @enderror
                    <input type="password" placeholder="@lang('Confirm Password')" name="password_confirmation"
                           id="password_confirmation">

                    <div class="form-check align-self-start">
                        <label class="form-check-label" for="flexCheckDefault">
                            @lang('I Agree with the Terms & conditions')
                        </label>
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"/>

                    </div>

                    <input type="submit" value="@lang('sign up')">
                    <div class="group">

                        <a href="{{route('login')}}">@lang('Login here')</a>
                    </div>

                    @if(basicControl()->reCaptcha_status_registration)
                        <div class="col-12 box mb-2 input-box">
                            {!! NoCaptcha::renderJs(session()->get('trans')) !!}
                            {!! NoCaptcha::display($basic->theme == 'msader' ? ['data-theme' => 'msader'] : []) !!}
                            @error('g-recaptcha-response')
                            <span class="text-danger mt-1">@lang($message)</span>
                            @enderror
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection
