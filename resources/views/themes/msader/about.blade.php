@extends('themes.msader.layouts.guest')
@section('title','About Us')

@section('content')
</br></br>
    @include('themes.msader.sections.about')
    @include('themes.msader.sections.testimonial')

@endsection
