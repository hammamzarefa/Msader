@extends('themes.msader.layouts.guest')
@section('content')

    @if(isset($contentDetails['tinker']))
        <!-- Start Ticker -->
        @include('themes.msader.sections.ticker')
        <!-- End Ticker -->
    @endif

    <!-- Start Slider -->
    @include('themes.msader.partials.heroBanner')
    <!-- End Slider -->
    <!-- Start Testimonial -->
    @include('themes.msader.sections.testimonial')
    <!-- End Testimonial -->
    <!-- Start Services -->
    @include('themes.msader.sections.service')
    <!-- End Services -->
    <!-- Start Video -->
    @include('themes.msader.sections.video')
    <!-- End Video -->
    <!-- Start News -->
    @include('themes.msader.sections.blog')
    <!-- End News -->
@endsection
@push('js')
    <script>
        AOS.init();
        const swiper = new Swiper('.swiper', {
            effect: "cards",
            loop: true,
            grabCursor: true,
            cardsEffect: {
                slideShadows: false,
            },

            autoplay: {delay: 5000},

        });
    </script>
@endpush
