@if(isset($contentDetails['testimonial']))
    <div class="testimonial mt-5">
        @if(isset($templates['testimonial'][0]) && $testimonial = $templates['testimonial'][0])
            <section class="uniq-title">
                <div class="top">@lang($testimonial->description->title)</div>
                <div class="bottom" aria-hidden="true">@lang($testimonial->description->title)</div>
            </section>
        @endif
        <div class="container-fluid">
            <div class="row">
                @foreach($contentDetails['testimonial'] as $key=>$data)
                    <div class="col-md-4 text-center mb-5" data-aos="fade-up" data-aos-duration="2500" data-aos-offset="100"
                         data-aos-delay="100">
                        <figure class="figure-card">
                            <blockquote>@lang(@$data->description->description)
                                <div class="arrow"></div>
                            </blockquote>
                            <img
                                src="{{getFile(config('location.content.path').@$data->content->contentMedia->description->image)}}"
                                alt="sq-sample3"/>
                            <div class="author">
                                <h5>@lang(@$data->description->name)
                                    <br><span> @lang(@$data->description->designation)</span></h5>
                            </div>
                        </figure>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
