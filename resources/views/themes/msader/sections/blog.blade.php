@if(isset($contentDetails['blog']))
<div class="news mt-5">
    @if(isset($templates['blog'][0]) && $blog = $templates['blog'][0])
    <section class="uniq-title">
        <div class="top">@lang($blog->description->title)</div>
        <div class="bottom" aria-hidden="true">@lang($blog->description->title)</div>
    </section>
    @endif
    <div class="container-fluid mt-5">
        <div class="row">
            @foreach($contentDetails['blog']->take(3)->sortDesc() as $data)
            <div class="col-12 col-sm-6 col-md-3" data-aos="zoom-in" data-aos-duration="2500" data-aos-offset="100"
                 data-aos-delay="100">
                <div class="post">
                    <div class="header_post">
                        <img src="{{getFile(config('location.content.path').'thumb_'.@$data->content->contentMedia->description->image)}}" alt="">
                    </div>
                    <div class="body_post">
                        <div class="post_content">
                           <div>
                            <h1>{{@$data->description->title}}</h1>
                            <p>{{Str::limit($data->description->title,80)}}</p>
                           </div>
                            <div>
                                <a class="breadcrumb-button see-more"
                                   href="{{route('blogDetails',[slug($data->description->title), $data->content_id])}}" style="--color: #a232a7;">
                                    @lang('Read More')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
@endif
