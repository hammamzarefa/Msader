<div id="video-content">
    <video id="video-target" width="400" autoplay muted loop>
        <source src="{{asset('assets/themes/user-v2/video.mp4')}}" type="video/mp4">
    </video>
    <div class="video-btn-element" id="video-btn-element">
        <img class="video-btn" data-src="{{asset('assets/themes/user-v2/img/others/video-btn.png')}}" alt="">
        <i class="fi-sr-play"></i>
    </div>
</div>
@push('js')
    <script>
        $(document).on('load', function () {
            footer_lazyloader();
        });

        function footer_lazyloader() {
            $("video.video-btn-element source").each(function () {
                var sourceFile = $(this).attr('data-src');
                $(this).attr('src', sourceFile);
            });
        }
    </script>
@endpush
