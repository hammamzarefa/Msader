<div class="ticker-conatin">
    <div class="ticker-wrap">
        <div class="ticker">
            @foreach($contentDetails['tinker'] as $k => $data)
                <div class="ticker__item">@lang(@$data->description->title)</div>
                <img width="40" src="{{asset('assets/themes/user-v2/img/icons/cat_001.png')}}" alt="">
            @endforeach
        </div>
    </div>
</div>
