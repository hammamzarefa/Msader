@if(isset($contentDetails['service']))
    <div class="services">
        <div class="container-fluid">
            @if(isset($templates['service'][0]) && $service = $templates['service'][0])
                <section class="uniq-title">
                    <div class="top">@lang(@$service->description->title)</div>
                    <div class="bottom" aria-hidden="true">@lang(@$service->description->title)</div>
                </section>
            @endif
            <div class="gradient-cards">
                @foreach($contentDetails['service'] as $data)
                    <div class="card" data-aos="flip-up" data-aos-duration="2500" data-aos-offset="100"
                         data-aos-delay="100">
                        <div class="container-card bg-green-box">
                            <img src="{{getFile(config('location.content.path').@$data->content->contentMedia->description->image)}}" alt="">
                            <p class="card-title">@lang(@$data->description->title)</p>
                            <p class="card-description">{{@$data->content->contentMedia->description->button_link}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
