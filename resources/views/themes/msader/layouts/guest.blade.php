<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    @include('user.layouts.head')
</head>
<body class="lang-{{ str_replace('_', '-', app()->getLocale()) }}">
@include('user.layouts.notification')
<!-- preloader -->
<div id="preloader">
    <div class="container d-flex justify-content-center align-items-center h-100">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
             x="0px" y="0px"
             width="300px" height="300px" viewBox="0 0 300 300" enable-background="new 0 0 300 300"
             xml:space="preserve">
                <g id="alchemist-logo">
                    <g class="alchemist-ph">
                        <g id="logo" transform="translate(62.000000, 27.000000)">
                            <defs>
                                <clipPath id="clipper">
                                    <polygon id="clip"
                                             points="131.662,175.813 131.662,100.285 44.016,142.285 44.016,175.813 87.839,198.578 					"/>
                                </clipPath>
                            </defs>
                            <g class="clipped">
                                <path class="chem2" d="M137.793,314.941c-14.552,29.618-50.691,41.945-80.309,27.393L4.521,316.311
                          c-29.618-14.553-41.944-50.691-27.392-80.31L3.152,183.04c14.552-29.618,50.692-41.944,80.31-27.392l52.961,26.021
                          c29.617,14.553,41.944,50.692,27.392,80.311L137.793,314.941z"/>
                                <path class="chem1" d="M36.142,314.941c14.552,29.618,50.691,41.945,80.31,27.393l52.963-26.023
                          c29.617-14.553,41.943-50.692,27.391-80.31l-26.021-52.961c-14.553-29.618-50.691-41.944-80.31-27.392L37.513,181.67
                          C7.895,196.223-4.432,232.362,10.12,261.98L36.142,314.941z"/>
                            </g>

                            <g id="pinas" transform="translate(60.314607, 84.300601)">
                                <polygon id="sun" fill="#6E41B5" points="23.598,29.126 17.859,23.812 19.608,16.199 27.093,13.903 32.828,19.219 31.082,26.83
                                  "/>
                                <polygon id="star3" fill="#6E41B5" points="30.482,45.43 27.343,42.522 28.301,38.357 32.394,37.101 35.535,40.009
                          34.578,44.174 				"/>
                                <polygon id="star2" fill="#6E41B5" points="43.148,3.049 42.348,7.247 38.301,8.654 35.056,5.862 35.857,1.664 39.904,0.261
                                  "/>
                                <polygon id="star1" fill="#6E41B5" points="1.405,12.744 5.523,11.563 8.607,14.528 7.575,18.676 3.456,19.857 0.374,16.891
                                  "/>
                            </g>
                        </g>
                    </g>
                </g>
                </svg>
    </div>

</div>
<!-- main-content -->
<div class="main-content" style="padding: 0;">
    <!-- header-area -->
    @include('user.layouts.header')
    <!-- header-area-end -->
    <!-- main-area -->
    <main style="margin-top: 130px;">
        @yield('content')
    </main>
    <!-- main-area-end -->
    @include('user.layouts.footer')
</div>

<!-- JS here -->
<script src="{{asset('assets/themes/user-v2/js/vendor/jquery-3.6.0.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/bootstrap.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/isotope.pkgd.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/slick.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/wow.min.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/plugins.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/js/main.js') }}"></script>
<script src="{{asset('assets/themes/user-v2/package/swiper-bundle.min.js') }}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="{{ asset('assets/global/js/axios.min.js') }}"></script>
<script src="{{ asset('assets/global/js/vue.min.js') }}"></script>
<script src="{{ asset('assets/global/js/pusher.min.js') }}"></script>
<script>
    AOS.init();
</script>
@stack('js')
</body>

