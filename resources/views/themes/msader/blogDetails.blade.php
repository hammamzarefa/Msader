@extends('themes.msader.layouts.guest')
@section('title','Blog Details')

@section('content')
    <section class="blog-details-area pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-8">
                    <div class="standard-blog-item blog-details-content">
                        <div class="blog-thumb">
                            <img src="{{$singleItem['image']}}" alt="">
                        </div>
                        <div class="standard-blog-content">
                            <ul class="standard-blog-meta">
                                <li><a href="#"><i class="flaticon-supermarket"></i>NFT Marketplace</a></li>
                                <li><a href="#"><i class="flaticon-avatar"></i>Admin</a></li>
                                <li><a href="#"><i class="flaticon-calendar"></i>{{$singleItem['M'].'  '.$singleItem['d']}}</a></li>
                            </ul>
                            <h4 class="title">{{$singleItem['title']}}</h4>
                            <p>@lang($singleItem['description'])</p>
{{--                            <blockquote>--}}
{{--                                “ Wallet that is functional for NFT purcasin You have Coinbase account at this point.Lorem ipsum nsectetur. Non fungible tokens or NFTs are cryptographic assets on a blockchain with unique identification codes “--}}
{{--                                <footer>jon Bernthal</footer>--}}
{{--                            </blockquote>--}}
{{--                            <p>Mallet that is functional for NFT purcasin You have Coinbase account at this point.Lorem ipsum nsectetur. Non fungible tokens or NFTs are cryptographic assets on a blockchain with unique identification codes a metadata that distinguish them from each other Unlike cryptocurren.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>--}}
{{--                            <h4 class="small-title">The online shop for the video</h4>--}}
{{--                            <p>Gallet that is functional for NFT purcasin You have Coinbase account at this point.Lorem ipsum nsectetur. Non fungible tokens or NFTs are cryptographic assets on a blockchain with unique identification codes a metadata that distinguish them from each other Unlike cryptocurren.</p>--}}
{{--                            <p>Wallet that is functional for NFT purcasin You have Coinbase account at this point.Lorem ipsum nsectetur. Non fungible tokens or NFTs are cryptographic assets on a blockchain with unique identification codes a metadata that distinguish them from each other Unlike cryptocurren.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>--}}
                            <div class="blog-line"></div>
                            <div class="blog-details-bottom">
                                <div class="blog-details-tags">
                                    <ul>
                                        <li class="title">Tags :</li>
                                        <li><a href="#">Business,</a></li>
                                        <li><a href="#">Work,</a></li>
                                        <li><a href="#">Knowledge,</a></li>
                                        <li><a href="#">Online</a></li>
                                    </ul>
                                </div>
                                <div class="blog-details-social">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="avatar-post mt-50 mb-50">
                        <div class="post-avatar-img">
                            <img src="assets/img/blog/post_avatar_img.png" alt="img">
                        </div>
                        <div class="post-avatar-content">
                            <h5>Thomas Herlihy</h5>
                            <p>Non-core vaccines are given dependng on the dog's exposure risk. These include vaccines against Bordetella.
                            </p>
                            <ul class="post-avatar-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
{{--                    <div class="blog-next-prev">--}}
{{--                        <ul>--}}
{{--                            <li class="blog-prev">--}}
{{--                                <a href="#"><img src="assets/img/icons/left_arrow.png" alt="img">Previous Post</a>--}}
{{--                            </li>--}}
{{--                            <li class="blog-next">--}}
{{--                                <a href="#">Next Post<img src="assets/img/icons/right_arrow.png" alt="img"></a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <div class="comment-reply-box">--}}
{{--                        <h5 class="title">Leave a Reply</h5>--}}
{{--                        <form action="#" class="comment-reply-form">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <div class="form-grp">--}}
{{--                                        <input type="text" placeholder="Author *">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <div class="form-grp">--}}
{{--                                        <input type="email" placeholder="Your Email *">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-grp">--}}
{{--                                <textarea name="message" placeholder="Type Comment Here..."></textarea>--}}
{{--                            </div>--}}
{{--                            <button type="submit" class="btn">Submit now</button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-xxl-3 col-lg-4">--}}
{{--                    <aside class="blog-sidebar">--}}
{{--                        <div class="widget">--}}
{{--                            <h4 class="sidebar-title">Search</h4>--}}
{{--                            <div class="sidebar-search">--}}
{{--                                <form action="#">--}}
{{--                                    <input type="text" placeholder="Search ...">--}}
{{--                                    <button type="submit"><i class="fa fa-search"></i></button>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="widget">--}}
{{--                            <h4 class="sidebar-title">Categories</h4>--}}
{{--                            <div class="sidebar-cat-list">--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#">Domain Names <i class="fas fa-angle-double-right"></i></a></li>--}}
{{--                                    <li><a href="#">Photography <i class="fas fa-angle-double-right"></i></a></li>--}}
{{--                                    <li><a href="#">Coinbase <i class="fas fa-angle-double-right"></i></a></li>--}}
{{--                                    <li><a href="#">Trading Cards <i class="fas fa-angle-double-right"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="widget">--}}
{{--                            <h4 class="sidebar-title">Recent Post</h4>--}}
{{--                            <div class="rc-post-list">--}}
{{--                                <ul>--}}
{{--                                    <li>--}}
{{--                                        <div class="rc-post-thumb">--}}
{{--                                            <a href="blog-details.html"><img src="assets/img/blog/rc_post_thumb01.jpg" alt=""></a>--}}
{{--                                        </div>--}}
{{--                                        <div class="rc-post-content">--}}
{{--                                            <ul class="standard-blog-meta">--}}
{{--                                                <li><a href="#"><i class="flaticon-avatar"></i>Admin</a></li>--}}
{{--                                                <li><a href="#"><i class="flaticon-calendar"></i>Mar 10, 2022</a></li>--}}
{{--                                            </ul>--}}
{{--                                            <h5 class="title"><a href="blog-details.html">Marketplace is the online shop for the video</a></h5>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <div class="rc-post-thumb">--}}
{{--                                            <a href="blog-details.html"><img src="assets/img/blog/rc_post_thumb02.jpg" alt=""></a>--}}
{{--                                        </div>--}}
{{--                                        <div class="rc-post-content">--}}
{{--                                            <ul class="standard-blog-meta">--}}
{{--                                                <li><a href="#"><i class="flaticon-avatar"></i>Admin</a></li>--}}
{{--                                                <li><a href="#"><i class="flaticon-calendar"></i>Mar 10, 2022</a></li>--}}
{{--                                            </ul>--}}
{{--                                            <h5 class="title"><a href="blog-details.html">Marketplace is the online shop for the video</a></h5>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </aside>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>

    <!-- BLOG -->
{{--    <section id="blog">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}


{{--                @if(isset($popularContentDetails['blog']))--}}
{{--                    <div class="col-lg-4">--}}
{{--                        <div class="blog-sidebar wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.35s">--}}

{{--                            <h5 class="h5 mt-40">{{trans('Popular Post')}}</h5>--}}

{{--                            <hr class="mt-20 mb-20 border">--}}

{{--                            <div class="popular-post">--}}

{{--                                @foreach($popularContentDetails['blog']->sortDesc() as $data)--}}
{{--                                    <div class="media align-items-center">--}}
{{--                                        <div class="media-img">--}}
{{--                                            <img class="br-4 popular-post-img"--}}
{{--                                                 src="{{getFile(config('location.content.path').'thumb_'.@$data->content->contentMedia->description->image)}}"--}}
{{--                                                 alt="{{@$data->description->title}}">--}}
{{--                                        </div>--}}
{{--                                        <div class="media-body ml-20">--}}
{{--                                            <p class="post-date mb-5">{{dateTime($data->created_at,'d M, Y')}}</p>--}}
{{--                                            <h6 class="h6">--}}
{{--                                                <a href="{{route('blogDetails',[slug($data->description->title), $data->content_id])}}">{{\Str::limit($data->description->title,40)}}</a>--}}
{{--                                            </h6>--}}

{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}

{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- /BLOG -->
@endsection
