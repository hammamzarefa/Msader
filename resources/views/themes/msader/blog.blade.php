@extends('themes.msader.layouts.guest')
@section('title')
    @lang($title)
@endsection

@section('content')
    @include('themes.msader.sections.blog')

@endsection
