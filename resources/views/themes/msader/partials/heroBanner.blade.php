{{--@if(isset($templates['hero'][0]) && $hero = $templates['hero'][0])--}}
{{--    <section class="home-section">--}}
{{--        <div class="overlay h-100">--}}
{{--        <div class="container h-100">--}}
{{--            <div class="row h-100 align-items-center gy-5 g-lg-4">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="text-box">--}}
{{--                    <h2>@lang($hero->description->title)</h2>--}}
{{--                    <p>@lang($hero->description->short_description)</p>--}}
{{--                    <a href="{{@$hero->templateMedia()->button_link}}" class="btn-Msader">--}}
{{--                        @lang($hero->description->button_name)--}}
{{--                    </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="img-box">--}}
{{--                    <img src="{{getFile(config('location.content.path').@$hero->templateMedia()->image)}}" alt="@lang('Hero Img')" class="img-fluid" />--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--@endif--}}
<div class="slider">
    <div class="row">
        <div class="col-md-6">
            <div class="swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper ">
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <div class="image">
                            <img src="{{asset('assets/themes/user-v2/img/bg/dark_rider-cover.webp')}}" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="image">
                            <img src="{{asset('assets/themes/user-v2/img/bg/force_mage-cover.webp')}}" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="image">
                            <img src="{{asset('assets/themes/user-v2/img/bg/card-cover.webp')}}" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 content-left ">
            <h2 class="title">
                طابت بروض الحياة كل <br> <span>مصادر</span></h2>
            <h5>كل ما تحتاجه من مصادر</h5>
            <div class="btn-slider">
                <a class="breadcrumb-button" href="#" style="--color: #19d8ad;">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    المزيد
                </a>
            </div>
        </div>
    </div>
</div>
