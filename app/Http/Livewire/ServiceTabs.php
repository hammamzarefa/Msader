<?php

namespace App\Http\Livewire;

use App\Http\Traits\Notify;
use App\Jobs\Notification as JobsNotification;
use App\Models\Admin;
use App\Models\NotifyTemplate;
use App\Models\Order;
use App\Models\Service;
use App\Models\SiteNotification;
use App\Notifications\ErrorsNotificationTelegram;
use App\Notifications\TelegramNotification;
use App\Services\TransactionService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class ServiceTabs extends Component
{
    use Notify;

    public $quantity;
    public $player_number;
    public $link;
    public $runs;
    public $interval;
    public $drip_feed;
    public $category = null;

    public $categories;

    public $services = [];
    public $current_service = null;


    protected $listeners = ['categorySelected'];

    public function render()
    {
        return view('user.livewire.service-tabs');
    }

    public function categorySelected($c_id)
    {
        $this->dispatchBrowserEvent('destroy');
        $this->services = [];
        $this->current_service = null;

        $this->category = $c_id['c_id'];
        $this->services = Service::userRate()->where('category_id', $c_id['c_id'])->where('service_status', 1)->get();
        $this->dispatchBrowserEvent('init', ['next' => false]);
    }

    public function setCurrentTo($s_id = null)
    {
        $this->resetErrorBag();
        $this->current_service = $s_id;
        $this->quantity = 0;
        $this->player_number = null;
        $this->link = null;
        $this->runs = null;
        $this->interval = null;
        $this->drip_feed = null;

    }


    public function createOrder()
    {


        $user = Auth::user();


        $validate = $this->validateOrder();
        $service = Service::userRate()->findOrFail($this->current_service);
        $basic = (object)config('basic');
        $orderData = $this->setOrderData($service);
        if ($service->min_amount <= $orderData['quantity'] && $service->max_amount >= $orderData['quantity']) {
            $quantity = $orderData['quantity'];
            $price = $orderData['price'];
            if ($user->balance < $price) {

                $this->addError('quantity', 'Insufficient balance in your wallet.');
                return;
            }

            DB::beginTransaction();
            try {
                //create new ordrer without save
                $order = $this->createOrderModel($orderData, $user);
                $order->save();

                //proccessing provider order
                if (isset($service->api_provider_id) && $service->api_provider_id != 0) {
                    //  $this->apiProviderOrder($service, $req, $order); /// TODO
                }
                $order->save();
                if ($service->category->type != "NUMBER") {
                    $user->balance -= $price;
                    $user->save();
                    $transaction = (new TransactionService)->createTransaction($user, $price, 'Place order' . $order->id, '-');
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $this->adminPushNotificationError($e);
                Log::error($e->getMessage());
                $this->addError('quantity', "There are some error . " . $e->getMessage());
                return;
            }

            $msg = [
                'username' => $user->username,
                'price' => $price,
                'currency' => $basic->currency
            ];
            $action = [
                "link" => route('admin.order.edit', $order->id),
                "icon" => "fas fa - cart - plus text - white"
            ];

            $data = [
                'service_name' => $service->category->category_title,
                'user' => $user->username,
                'quantity' => $orderData['quantity'],
            ];
            //$this->adminPushNotification('ORDER_CREATE', $msg, $action, $data);


            $this->sendMailSms($user, 'ORDER_CONFIRM', [
                'order_id' => $order->id,
                'order_at' => $order->created_at,
                'service' => optional($order->service)->service_title,
                'status' => $order->status,
                'paid_amount' => $price,
                'remaining_balance' => $user->balance,
                'currency' => $basic->currency,
                'transaction' => @$transaction->trx_id,
            ]);

            session()->flash('success', 'Your order has been submitted');
            $this->dispatchBrowserEvent('back');
        } else {
            $this->addError('quantity', "Order quantity should be minimum {
                    $service->min_amount} and maximum {
                    $service->max_amount}");

        }


    }

    private function validateOrder(): array
    {

        $rules = [
            'category' => 'required|integer|min:1|not_in:0',
            'current_service' => 'required|integer|min:1|not_in:0',
            'link' => 'required',
            'quantity' => 'required|integer',
            //'check' => 'required',
        ];
        if (!$this->drip_feed) {
            $rules['runs'] = 'required|integer|not_in:0';
            $rules['interval'] = 'required|integer|not_in:0';
        }
        $this->validate($rules);


        return [];
    }

    public function setOrderData($service)
    {

        $basic = (object)config('basic');
        $orderData['category'] = $service->category;
        if ($service->category->type == "NUMBER") {
            $orderData['quantity'] = 1;
        } else {
            $orderData['quantity'] = $this->quantity;
        }
        $orderData['quantity'] = $this->quantity;
        if ($service->drip_feed == 1) {
            if (!$this->drip_feed) {
                $rules['runs'] = 'required|integer|not_in:0';
                $rules['interval'] = 'required|integer|not_in:0';
                $validator = $this->validate($rules);

                $orderData['quantity'] = $this->quantity * $this->runs;
            }
        }
        $userRate = ($service->user_rate) ?? $service->price;
        if ($service->category->type == "SMM") {
            $orderData['price'] = round(($orderData['quantity'] * $userRate) / 1000, $basic->fraction_number);
        } else {
            $orderData['price'] = round(($orderData['quantity'] * $userRate), $basic->fraction_number);
        }
        return $orderData;

    }

    public function createOrderModel($orderData, $user): Order
    {
        $order = new Order();
        $order->user_id = $user->id;
        $order->category_id = $orderData['category'];
        $order->service_id = $this->current_service;
        $order->link = $this->link ?? '';
        $order->quantity = $orderData['quantity'];
        $order->status = 'processing';
        $order->price = $orderData['price'];
        $order->runs = $this->runs;
        $order->interval = $this->interval;
        return $order;
    }

    public function adminPushNotificationError($e)
    {
        new ErrorsNotificationTelegram($e->getMessage());
    }

    public function adminPushNotification($templateKey, $params = [], $action = [], $data = [])
    {

        $basic = (object)config('basic');
        if ($basic->push_notification != 1) {
            return false;
        }

        $templateObj = NotifyTemplate::where('template_key', $templateKey)->where('status', 1)->first();

        if (!$templateObj) {
            return false;
        }

        if ($templateObj) {
            $template = $templateObj->body;
            foreach ($params as $code => $value) {
                $template = str_replace('[[' . $code . ']]', $value, $template);
            }
            $action['text'] = $template;
        }
        $admins = Admin::all();
        foreach ($admins as $admin) {
            $siteNotification = new SiteNotification();
            $siteNotification->description = $action;
            $admin->siteNotificational()->save($siteNotification);

            event(new \App\Events\AdminNotification($siteNotification, $admin->id));
        }
        new TelegramNotification($siteNotification->description, $data);
        JobsNotification::dispatch($action, $data);
    }
}
