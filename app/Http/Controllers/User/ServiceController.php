<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ContentDetails;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ServiceController extends Controller
{
    public function index()
    {
        $categories = Category::where('status', 1)->get();
        $banner = Cache::rememberForever('banner', function () {
            return ContentDetails::select('id', 'content_id', 'description', 'created_at')
                ->whereHas('content', function ($query) {
                    return $query->where('name', 'banner');
                })
                ->with(['content:id,name',
                    'content.contentMedia' => function ($q) {
                        $q->select(['content_id', 'description']);
                    }])
                ->first();
        });
        return view('user.pages.services.show-service', compact('categories','banner'));
    }


    public function search(Request $request)
    {
        $categories = Category::with('service')->where('status', 1)->get();
        $search = $request->all();
        $services = Service::where('service_status', 1)
            ->userRate()
            ->when(isset($search['service']), function ($query) use ($search) {
                return $query->where('service_title', 'LIKE', "%{$search['service']}%");
            })
            ->when(isset($search['category']), function ($query) use ($search) {
                return $query->where('category_id', $search['category']);
            })
            ->with(['category'])
            ->get()
            ->groupBy('category.category_title');
        return view('user.pages.services.search-service', compact('services', 'categories'));
    }

    public function services($id)
    {
        $services = Service::userRate()->with('category:id,category_title,type,image')->where('category_id', $id)->where('service_status', 1)->get();
        $current_service = null;
        return view('user.pages.services.show-service-by-category', compact('services','current_service'));

    }
    public function order($id)
    {
        $service = Service::userRate()->where('service_status', 1)->find($id);
        return view('user.pages.order.new', compact('service'));

    }
}
