<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class ImportMultiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'services' => 'required|json',
            'import_quantity' => 'required|in:all,' . implode(',', range(25, 1000, 25)),
            'price_percentage_increase' => 'required|numeric|min:0',
            'provider' => 'required|exists:api_providers,id',
            'category_id' => 'nullable|exists:categories,id',
        ];
    }
}
