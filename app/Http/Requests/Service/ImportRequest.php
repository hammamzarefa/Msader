<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class ImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'category_id' => 'nullable|exists:categories,id',
            'category' => 'nullable|string|max:255',
            'min' => 'nullable|integer|min:1',
            'max' => 'nullable|integer|min:1',
            'rate' => 'nullable|numeric|min:0',
            'price_percentage_increase' => 'required|numeric|min:0',
            'provider' => 'required|exists:api_providers,id',
            'id' => 'required'
        ];
    }
}
