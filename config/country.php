<?php

return [
    [
        "name" => "Syria",
        "code" => "SY",
        "phone_code" => "+963"
    ],
    [
        "name" => "Algeria",
        "code" => "DZ",
        "phone_code" => "+213"
    ],
    [
        "name" => "Bahrain",
        "code" => "BH",
        "phone_code" => "+973"
    ],
    [
        "name" => "Egypt",
        "code" => "EG",
        "phone_code" => "+20"
    ],
    [
        "name" => "Iraq",
        "code" => "IQ",
        "phone_code" => "+964"
    ],
    [
        "name" => "Jordan",
        "code" => "JO",
        "phone_code" => "+962"
    ],
    [
        "name" => "Kuwait",
        "code" => "KW",
        "phone_code" => "+965"
    ],
    [
        "name" => "Lebanon",
        "code" => "LB",
        "phone_code" => "+961"
    ],
    [
        "name" => "Libya",
        "code" => "LY",
        "phone_code" => "+218"
    ],
    [
        "name" => "Morocco",
        "code" => "MA",
        "phone_code" => "+212"
    ],
    [
        "name" => "Oman",
        "code" => "OM",
        "phone_code" => "+968"
    ],
    [
        "name" => "Qatar",
        "code" => "QA",
        "phone_code" => "+974"
    ],
    [
        "name" => "Saudi Arabia",
        "code" => "SA",
        "phone_code" => "+966"
    ],
    [
        "name" => "Tunisia",
        "code" => "TN",
        "phone_code" => "+216"
    ],
    [
        "name" => "Turkey",
        "code" => "TR",
        "phone_code" => "+90"
    ],
    [
        "name" => "United Arab Emirates",
        "code" => "AE",
        "phone_code" => "+971"
    ],
    [
        "name" => "Yemen",
        "code" => "YE",
        "phone_code" => "+967"
    ]
];
